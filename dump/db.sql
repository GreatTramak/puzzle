-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 12 2017 г., 21:27
-- Версия сервера: 5.5.53
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `puzzle`
--

-- --------------------------------------------------------

--
-- Структура таблицы `labyrinths`
--

CREATE TABLE `labyrinths` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `specials` text COLLATE utf8_unicode_ci,
  `accessible_way` text COLLATE utf8_unicode_ci,
  `solution` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `m` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `n` tinyint(3) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `labyrinths`
--

INSERT INTO `labyrinths` (`id`, `specials`, `accessible_way`, `solution`, `status`, `m`, `n`) VALUES
(1, '{\"cellMatrix\":[[0,0,\"t1\",0],[0,\"t2\",0,0],[0,0,\"t2\",0],[\"t1\",0,0,0]],\"pathMatrix\":[],\"start\":[{\"m\":5,\"n\":1}],\"end\":[{\"m\":1,\"n\":4,\"dm\":-1,\"dn\":0}]}', '{\"horizont\":[[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]],\"vertical\":[[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1]]}', 'a_5_1:b_4_2:b_3_2:b_2_2:a_2_2:a_2_3:b_1_4:a_1_4', 1, 4, 4),
(8, '{\"cellMatrix\":[[\"t1\"],[\"t2\"]],\"pathMatrix\":[],\"start\":[{\"m\":2,\"n\":1}],\"end\":[{\"m\":2,\"n\":2,\"dm\":0,\"dn\":1}]}', '{\"horizont\":[[1],[1],[1]],\"vertical\":[[1,1],[1,1]]}', 'a_2_1', 1, 2, 1),
(9, '{\"cellMatrix\":[[\"t1\"],[\"t2\"]],\"pathMatrix\":[],\"start\":[{\"m\":3,\"n\":1}],\"end\":[{\"m\":1,\"n\":2,\"dm\":-1,\"dn\":1}]}', '{\"horizont\":[[1],[1],[1]],\"vertical\":[[1,1],[1,1]]}', 'b_2_1:a_2_1:b_1_2', 1, 2, 1),
(10, '{\"cellMatrix\":[[\"t1\"],[\"t1\"],[\"t2\"]],\"pathMatrix\":[],\"start\":[{\"m\":4,\"n\":1}],\"end\":[{\"m\":1,\"n\":2,\"dm\":-1,\"dn\":1}]}', '{\"horizont\":[[1],[1],[1],[1]],\"vertical\":[[1,1],[1,1],[1,1]]}', 'b_3_1:a_3_1:b_2_2:b_1_2', 1, 3, 1),
(11, '{\"cellMatrix\":[[\"t1\",\"t1\"],[\"t1\",\"t2\"]],\"pathMatrix\":[],\"start\":[{\"m\":3,\"n\":1}],\"end\":[{\"m\":1,\"n\":3,\"dm\":-1,\"dn\":1}]}', '{\"horizont\":[[1,1],[1,1],[1,1]],\"vertical\":[[1,1,1],[1,1,1]]}', 'a_3_1:b_2_2:a_2_2:b_1_3', 1, 2, 2),
(12, '{\"cellMatrix\":[[\"t2\",\"t2\",\"t2\"],[\"t2\",\"t1\",\"t2\"],[\"t1\",\"t1\",\"t1\"]],\"pathMatrix\":[],\"start\":[{\"m\":4,\"n\":1}],\"end\":[{\"m\":1,\"n\":4,\"dm\":-1,\"dn\":1}]}', '{\"horizont\":[[1,1,1],[1,1,1],[1,1,1],[1,1,1]],\"vertical\":[[1,1,1,1],[1,1,1,0],[1,1,1,1]]}', 'b_3_1:a_3_1:b_2_2:a_2_2:b_2_3:a_3_3:b_2_4:b_1_4', 1, 3, 3),
(15, '{\"cellMatrix\":[[\"t2\",0,\"t1\",\"t2\"],[\"t2\",\"t2\",\"t2\",\"t2\"],[\"t2\",\"t1\",\"t2\",\"t2\"],[\"t1\",\"t1\",\"t1\",\"t2\"]],\"pathMatrix\":[],\"start\":[{\"m\":5,\"n\":1}],\"end\":[{\"m\":1,\"n\":2,\"dm\":-1,\"dn\":0}]}', '{\"horizont\":[[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]],\"vertical\":[[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1]]}', 'b_4_1:a_4_1:b_3_2:a_3_2:b_3_3:a_4_3:b_4_4:a_5_4:b_4_5:b_3_5:b_2_5:b_1_5:a_1_4:b_1_4:a_2_3:a_2_2:b_1_2', 1, 4, 4),
(16, '{\"cellMatrix\":[[\"t2\",\"t2\",\"t1\",\"t2\"],[0,\"t2\",\"t2\",\"t2\"],[\"t2\",\"t1\",\"t2\",\"t2\"],[\"t1\",\"t1\",\"t1\",\"t2\"]],\"pathMatrix\":[],\"start\":[{\"m\":5,\"n\":1}],\"end\":[{\"m\":1,\"n\":5,\"dm\":0,\"dn\":1}]}', '{\"horizont\":[[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]],\"vertical\":[[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1]]}', 'a_5_1:a_5_2:a_5_3:b_4_4:a_4_3:b_3_3:a_3_2:b_3_2:a_4_1:b_3_1:b_2_1:b_1_1:a_1_1:a_1_2:b_1_3:a_2_3:b_1_4:a_1_4:b_1_5:b_2_5', 1, 4, 4),
(17, '{\"cellMatrix\":[[0,\"t2\",\"t1\",\"t2\"],[\"t2\",0,0,\"t2\"],[\"t2\",\"t1\",\"t2\",0],[\"t1\",\"t1\",\"t1\",\"t2\"]],\"pathMatrix\":[],\"start\":[{\"m\":5,\"n\":1}],\"end\":[{\"m\":1,\"n\":5,\"dm\":0,\"dn\":1}]}', '{\"horizont\":[[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]],\"vertical\":[[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1]]}', 'b_4_1:a_4_1:b_3_2:b_2_2:a_2_2:b_1_3:a_1_3:b_1_4:b_2_4:a_3_3:b_3_3:a_4_3:b_4_4', 1, 4, 4),
(18, '{\"cellMatrix\":[[\"t1\",\"t2\"]],\"pathMatrix\":[],\"start\":[{\"m\":2,\"n\":1}],\"end\":[{\"m\":1,\"n\":3,\"dm\":-1,\"dn\":1}]}', '{\"horizont\":[[1,1],[1,1]],\"vertical\":[[1,1,1]]}', 'a_2_1:b_1_2:a_1_2', 1, 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `labyrinth_types`
--

CREATE TABLE `labyrinth_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `labyrinth_types`
--

INSERT INTO `labyrinth_types` (`id`, `title`, `slug`, `status`) VALUES
(1, 'Чёрное и белое', 'blackwhite', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `labyrinth_types_relations`
--

CREATE TABLE `labyrinth_types_relations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `labyrinth_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` tinyint(4) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `labyrinth_types_relations`
--

INSERT INTO `labyrinth_types_relations` (`id`, `labyrinth_id`, `type_id`, `status`) VALUES
(1, 1, 1, 1),
(7, 8, 1, 1),
(8, 9, 1, 1),
(9, 10, 1, 1),
(10, 11, 1, 1),
(11, 12, 1, 1),
(14, 15, 1, 1),
(15, 16, 1, 1),
(16, 17, 1, 1),
(17, 18, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `matches`
--

CREATE TABLE `matches` (
  `id` int(10) UNSIGNED NOT NULL,
  `solution` text COLLATE utf8_unicode_ci,
  `initialization` text COLLATE utf8_unicode_ci,
  `difficult` tinyint(3) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_04_15_181538_create_pages_table', 2),
(4, '2016_06_10_191642_alter_table_pages', 2),
(5, '2017_01_25_180410_create_table_puzzles', 3),
(6, '2017_02_27_175009_create_labyrinth_table', 4),
(7, '2017_02_27_180033_create_labyrinth_users_relations_table', 5),
(8, '2017_02_27_181033_create_labyrinth_types_relations', 6),
(9, '2017_02_27_181312_create_labyrinth_types_tables', 7),
(10, '2017_02_27_181635_create_puzzles_table', 8),
(11, '2017_02_27_182328_create_match_table', 9),
(12, '2017_02_27_182654_create_regions_table', 10),
(13, '2017_03_05_132903_alter_labyrinth_table', 11),
(14, '2017_03_05_133341_alter_puzzles_table', 12),
(15, '2017_03_05_154206_alter_labyrinth_table2', 13);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `autorized` int(11) NOT NULL,
  `common` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `slug`, `status`, `title`, `autorized`, `common`) VALUES
(1, 'home', '1', 'Домашняя', 0, 1),
(2, 'about', '1', 'О проекте', 0, 1),
(3, 'journeys', '1', 'Головоломки', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `puzzles`
--

CREATE TABLE `puzzles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `category_puzzle_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `difficult` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `region_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `hash` varchar(40) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `puzzles`
--

INSERT INTO `puzzles` (`id`, `item_id`, `category_puzzle_id`, `difficult`, `region_id`, `status`, `hash`) VALUES
(1, 1, 1, 1, 1, 1, 'd4dfe0d38ada1ffdd72cdaa569078a61'),
(7, 8, 1, 1, 1, 1, 'ddcf1b57fe536b9f8297299c28c0a2fc'),
(8, 9, 1, 1, 1, 1, '02a80a57726cc6ae3078a7945f4c35d9'),
(9, 10, 1, 1, 1, 1, '0e0a4eb280283d343698dcbf9213bdb2'),
(10, 11, 1, 1, 1, 1, '98f03efa3211a0ecf36a927c48417063'),
(11, 12, 1, 1, 1, 1, 'bd6ff39dcf02ed8cd2470b5860b67d46'),
(14, 15, 1, 1, 1, 1, '8c3a114b384bfc4b2650d0ddd735ad3c'),
(15, 16, 1, 1, 1, 1, 'ec068c78df4ed5e8d1e7056e1324f705'),
(16, 17, 1, 1, 1, 1, '25f63ab2c3d43fa37b565fe5a6641659'),
(17, 18, 1, 1, 1, 1, '049b64ca1904c61d96575a3752c8a072');

-- --------------------------------------------------------

--
-- Структура таблицы `puzzles_category`
--

CREATE TABLE `puzzles_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `puzzles_category`
--

INSERT INTO `puzzles_category` (`id`, `slug`, `status`, `title`, `image`) VALUES
(1, 'labyrints', '1', 'Лабиринты', '');

-- --------------------------------------------------------

--
-- Структура таблицы `regions`
--

CREATE TABLE `regions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map_id` tinyint(3) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `level` smallint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `type`, `remember_token`, `created_at`, `updated_at`, `level`) VALUES
(1, 'tramak', 'greattramak@mail.ru', '$2y$10$shKpfi7Z89YxSa7.xFdWPeF97RBLH7yAeAe1SplT/G76Pl6N5Mkpi', 0, NULL, '2017-01-17 16:17:58', '2017-01-17 16:17:58', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users_puzzles`
--

CREATE TABLE `users_puzzles` (
  `id` int(10) UNSIGNED NOT NULL,
  `puzzle_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `solution` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `labyrinths`
--
ALTER TABLE `labyrinths`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `labyrinth_types`
--
ALTER TABLE `labyrinth_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `labyrinth_types_relations`
--
ALTER TABLE `labyrinth_types_relations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Индексы таблицы `puzzles`
--
ALTER TABLE `puzzles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `puzzles_category`
--
ALTER TABLE `puzzles_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Индексы таблицы `users_puzzles`
--
ALTER TABLE `users_puzzles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `labyrinths`
--
ALTER TABLE `labyrinths`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `labyrinth_types`
--
ALTER TABLE `labyrinth_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `labyrinth_types_relations`
--
ALTER TABLE `labyrinth_types_relations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `matches`
--
ALTER TABLE `matches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `puzzles`
--
ALTER TABLE `puzzles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `puzzles_category`
--
ALTER TABLE `puzzles_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `users_puzzles`
--
ALTER TABLE `users_puzzles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
