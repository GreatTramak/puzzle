<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabyrinthTypesRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labyrinth_types_relations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('puzzle_id');
            $table->tinyInteger('type_id');
            $table->tinyInteger('status');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
