<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGeneratorLabyrinth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generator_labyrinth', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->tinyInteger('m')->unsigned();
            $table->tinyInteger('n')->unsigned();
            $table->string('types');
            $table->boolean('generated')->default(false);
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
