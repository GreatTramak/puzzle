<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabyrinthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labyrinths', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('specials');
            $table->text('accessible');
            $table->text('solution');
            $table->tinyInteger('difficult');	
            $table->tinyInteger('status');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
