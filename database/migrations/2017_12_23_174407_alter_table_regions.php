<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maps', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('title', 255)->nullable();
            $table->string('slug', 255)->nullable();
            $table->tinyInteger('level_to_open')->unsigned();
            $table->tinyInteger('status')->default(1);
	});
        
        Schema::table('regions', function (Blueprint $table) {
            $table->tinyInteger('level_to_open')->unsigned();
            $table->string('url', 150)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
