<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateTablePuzzles
 */
class CreateTablePuzzles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puzzles_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 255);
            $table->string('status', 255);
            $table->string('title', 255);
            $table->string('image',255);	
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
