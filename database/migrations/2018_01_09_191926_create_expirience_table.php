<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpirienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_experience', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->tinyInteger('level')->unsigned();
            $table->integer('experience')->unsigned();
	});
        
        Schema::create('region_colors', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->tinyInteger('region_id')->unsigned();
            $table->string('color', 10)->nullable();
            $table->tinyInteger('status')->default(1);
	});
        
        Schema::table('users', function (Blueprint $table) {
            $table->integer('experience')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
