<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuzzlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puzzles', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('item_id')->unsigned();
            $table->integer('category_puzzle_id')->unsigned();
            $table->tinyInteger('difficult')->unsigned();
            $table->tinyInteger('region_id')->unsigned();
            $table->tinyInteger('status')->default(1);
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
