<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabyrinthUsersRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_puzzles', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('puzzle_id');
            $table->integer('user_id');
            $table->text('solution');
            $table->tinyInteger('status');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
