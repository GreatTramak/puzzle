<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\{CronController, HomeController, ImageController, LabyrinthController, MainController,
    UserController};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Navigation Routes
Route::get('/home', [HomeController::class, 'index']);

// Puzzle Routes
Route::get('/', [MainController::class, 'index']);
Route::get('/journeys/', [MainController::class, 'journeysPage']);

Route::get('/screensaver/', [ImageController::class, 'backgroundImage']);
Route::get('/labyrinth/completed/', [LabyrinthController::class, 'indexCompleted']);

Route::get('/regions/greenhall/', ['as' => 'greenhall', function () {
    $params = [
        'region_id' => 1
    ];
    return App::make(CONTROLLERS_PATH . 'LabyrinthController')->index($params);
}]);

Route::get('/regions/greenhall/{hash}', ['as' => 'greenhallView', function ($hash) {
    $params = [
        'region_id' => 1,
        'types' => ['blackwhite'],
        'hash' => $hash
    ];
    return App::make(CONTROLLERS_PATH . 'LabyrinthController')->view($params);
}]);

Route::get('/regions/forest/', ['as' => 'forest', function () {
    $params = [
        'region_id' => 2
    ];
    return App::make(CONTROLLERS_PATH . 'LabyrinthController')->index($params);
}]);

Route::get('/regions/forest/{hash}', ['as' => 'forestView', function ($hash) {
    $params = [
        'region_id' => 2,
        'types' => ['completepoints'],
        'hash' => $hash
    ];
    return App::make(CONTROLLERS_PATH . 'LabyrinthController')->view($params);
}]);

Route::get('/regions/marsh/', ['as' => 'marsh', function () {
    $params = [
        'region_id' => 3
    ];
    return App::make(CONTROLLERS_PATH . 'LabyrinthController')->index($params);
}]);

Route::get('/regions/marsh/{hash}', ['as' => 'marshView', function ($hash) {
    $params = [
        'region_id' => 3,
        'types' => ['blackwhite', 'completepoints'],
        'hash' => $hash
    ];
    return App::make(CONTROLLERS_PATH . 'LabyrinthController')->view($params);
}]);

Route::get('/regions/tropics/', ['as' => 'tropics', function () {
    $params = [
        'region_id' => 4
    ];
    return App::make(CONTROLLERS_PATH . 'LabyrinthController')->index($params);
}]);

Route::get('/regions/tropics/{hash}', ['as' => 'tropicsView', function ($hash) {
    $params = [
        'region_id' => 4,
        'types' => ['countside'],
        'hash' => $hash
    ];
    return App::make(CONTROLLERS_PATH . 'LabyrinthController')->view($params);
}]);

Route::get('/labyrinth/solutor/', [LabyrinthController::class, 'solutor']);
Route::post('/labyrinth/resolve', [LabyrinthController::class, 'resolve']);

// User Roures
Auth::routes();
Route::get('/loginOut', [UserController::class, 'logoutUser']);

// System Routes
// Clear Cache facade value:
Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');

    // Route cache cleared
    // Not Working!!!
    //$exitCode = Artisan::call('route:cache');

    // View cache cleared
    $exitCode = Artisan::call('view:clear');

    // Clear Config cleared
    $exitCode = Artisan::call('config:cache');

    // Reoptimized class loader
    $exitCode = Artisan::call('optimize');

    return '<h1>All Cache cleared and optimized</h1>';
});

// Очистка таблиц с головоломками - пока для разработки
Route::get('/system/clear_tables/', [CronController::class, 'clearDatabaseAction']);

// Cron Routes
Route::get('/cron/test/', [CronController::class, 'testAction']);
Route::get('/cron/generate_groups_labyrinth/', [CronController::class, 'generateGroupAction']);

Route::get('/cron/generate_labyrinth/', [CronController::class, 'generateAction']);
