<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8"/>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Puzzle - сайт для тех. кто хочет поломать голову</title>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="{{asset('js/lib/jquery.imagemapster.min.js')}}"></script>
        <script src="{{asset('js/lib/jquery.jcarousel.min.js')}}"></script>
        <script src="{{asset('js/lib/jquery.jcarousel.basic.js')}}"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        
        <!-- Fonts -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

        <!-- Styles -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{asset('css/common.css')}}" rel='stylesheet' type='text/css'>
        <link href="{{asset('css/lib/jcarousel.basic.css')}}" rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div id="header">
            <nav class="navbar navbar-default navbar-static-top no-margin">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav main_menu">
                            @if (isset($helper) && isset($helper['pages']))
                                @foreach($helper['pages'] as $p)
                                    <li><a href="/{{ $p['slug'] }}/">{{ $p['title'] }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                            <!-- Здесь при 404 ошибке показывается что пользователь - гость-->
                                <li><a href="{{ url('/login') }}">Login</a></li>
                                <li><a href="{{ url('/register') }}">Register</a></li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="/profile/{{ Auth::user()->id }}/"><i class="fa fa-btn fa-user"></i>Профиль</a></li>
                                        <li><a href="{{ url('/loginOut') }}"><i class="fa fa-btn fa-sign-out"></i>Выход</a></li>
                                    </ul>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
            
            <div class="header_text">
                @yield('header')
            </div>
        </div>
	<div id="content">
            @yield('content')
	</div>
	<div id="footer">
            @yield('footer')
            <div class="trademark">Puzzle (c) {{date('Y')}}</div>
	</div>
    </body>
</html>
<script src="{{asset('js/common.js')}}"></script>