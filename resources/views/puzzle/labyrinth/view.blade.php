@extends('puzzle.puzzleview')

@section('inner_puzzle')
    <div id="contextMenuLabyrinth">
        <ul>
            <li><a href="/" class="js-labyrinth-init">Заново</a></li>
            <li><a href="/">В основное меню</a></li>
        </ul>
    </div>
    <div class="puzzle_placeholder">
        {{ csrf_field() }}
        <input type="hidden" name="m" value="{{$params['m']}}"/>
        <input type="hidden" name="n" value="{{$params['n']}}"/>
        <input type="hidden" name="types" value="{{implode(",", $params['types'])}}"/>
        <input type="hidden" name="specials" value="{{json_encode($params['specials'])}}"/>
        <input type="hidden" name="id" value="{{$params['id']}}"/>
        <svg xmlns="http://www.w3.org/0000/svg" id="puzzle_draw">
            <!-- обычные линии-->
            <!--vertical lines-->
            @foreach($params['accessible_way']['vertical'] as $k_linev => $line)
                @foreach($line as $k_columnv => $column)
                    {{$x1 = ($params['offsetX'] + $k_columnv * $params['cellLength'])}}
                    {{$x2 = ($params['offsetX'] + $k_columnv * $params['cellLength'])}}
                    {{$y1 = ($params['offsetY'] + $k_linev * $params['cellLength'])}}
                    {{$y2 = ($params['offsetY'] + ($k_linev + 1) * $params['cellLength'])}}
                    @if($column == 1)
                        <line
                            x1="{{$x1}}" x2="{{$x2}}" y1="{{$y1}}" y2="{{$y2}}"
                            stroke-width="10"
                            stroke="grey"
                            stroke-linecap="round"
                            id="b_{{$k_linev + 1}}_{{$k_columnv + 1}}"
                            class="rib"/>
                    @else
                        <line x1="{{$x1}}" x2="{{$x2}}"
                            y1="{{$y1}}" y2="{{$y1 + (($y2-$y1) / 100) * $params['shatter']}}"
                            stroke-width="10"
                            stroke="grey"
                            stroke-linecap="round"
                            class="b_{{$k_linev + 1}}_{{$k_columnv + 1}}"/>
                        <line x1="{{$x1}}" x2="{{$x2}}"
                              y1="{{$y2 - (($y2-$y1) / 100) * $params['shatter']}}" y2="{{$y2}}"
                            stroke-width="10"
                            stroke="grey"
                            stroke-linecap="round"
                            class="b_{{$k_linev + 1}}_{{$k_columnv + 1}}"/>
                    @endif
                @endforeach
            @endforeach
            <!--horizont lines-->
            @foreach($params['accessible_way']['horizont'] as $k_line => $line)
                @foreach($line as $k_column => $column)
                    {{$x1 = ($params['offsetX'] + $k_column * $params['cellLength'])}}
                    {{$x2 = ($params['offsetX'] + ($k_column + 1) * $params['cellLength'])}}
                    {{$y1 = ($params['offsetY'] + $k_line * $params['cellLength'])}}
                    {{$y2 = ($params['offsetY'] + $k_line * $params['cellLength'])}}
                    @if($column == 1)
                        <line
                            x1="{{$x1}}" x2="{{$x2}}" y1="{{$y1}}" y2="{{$y2}}"
                            stroke-width="10"
                            stroke="grey"
                            stroke-linecap="round"
                            id="a_{{$k_line + 1}}_{{$k_column + 1}}"
                            class="rib"/>
                    @else
                        <line
                            x1="{{$x1}}" x2="{{$x1 + (($x2-$x1) / 100) * $params['shatter']}}"
                            y1="{{$y1}}" y2="{{$y2}}"
                            stroke-width="10"
                            stroke="grey"
                            stroke-linecap="round"
                            class="a_{{$k_line + 1}}_{{$k_column + 1}}"/>

                        <line
                            x1="{{$x2 - (($x2-$x1) / 100) * $params['shatter']}}" x2="{{$x2}}"
                            y1="{{$y1}}" y2="{{$y2}}"
                            stroke-width="10"
                            stroke="grey"
                            stroke-linecap="round"
                            class="a_{{$k_line + 1}}_{{$k_column + 1}}"/>
                    @endif
                @endforeach
            @endforeach

            <!--особенности-->
            <!--Точки на линиях-->
            @if(isset($params['specials']['pathMatrix']) && $params['specials']['pathMatrix'])
                @foreach($params['specials']['pathMatrix']['h'] as $k_line => $line)
                    @foreach($line as $k_column => $column)
                        @if($column === LABYRINTH_RED_RIB_POINT)
                            <circle
                                cx="{{($params['offsetX'] + $k_column * $params['cellLength']) + $params['cellLength'] / 2}}"
                                cy="{{$params['offsetY'] + ($k_line * $params['cellLength'])}}"
                                r="4"
                                style="stroke: black; fill: red;" />
                        @endif
                    @endforeach
                @endforeach
                @foreach($params['specials']['pathMatrix']['v'] as $k_line => $line)
                    @foreach($line as $k_column => $column)
                        @if($column === LABYRINTH_RED_RIB_POINT)
                            <circle
                                cx="{{$params['offsetX'] + $k_column * $params['cellLength']}}"
                                cy="{{$params['offsetY'] + ($k_line * $params['cellLength']) + $params['cellLength'] / 2}}"
                                r="4"
                                style="stroke: black; fill: red;" />
                        @endif
                    @endforeach
                @endforeach
            @endif
            <!--Точки на линиях-->
            <!--Конец особенностей-->

            <!--half-line-->
            <line
                x1="0" x2="0" y1="0" y2="0"
                stroke-width="10"
                stroke="yellow"
                stroke-linecap="round"
                class="rib"
                id="current_rib"
                style="display: none;"/>

            <!-- Точки выхода -->
            @foreach($params['specials']['end'] as $k_end => $end)
                <line
                    x1="{{($params['offsetX'] + ($end['n'] - 1) * $params['cellLength'])}}"
                    y1="{{($params['offsetY'] + ($end['m'] - 1) * $params['cellLength'])}}"
                    x2="{{($params['offsetX'] + (($end['n'] - 1) * $params['cellLength']) + ($end['dn'] * 10))}}"
                    y2="{{($params['offsetY'] + (($end['m'] - 1) * $params['cellLength']) + ($end['dm'] * 10))}}"
                    stroke-width="10" stroke-linecap="round" id="{{$end['m']}}_{{$end['n']}}"
                    style="stroke: grey; fill: grey; cursor: pointer"
                    class="exit" />
            @endforeach

            <!--светящиеся линии-->
            <!--vertical lines-->
            @foreach($params['accessible_way']['vertical'] as $k_linev => $line)
                @foreach($line as $k_columnv => $column)
                    {{$x1 = ($params['offsetX'] + $k_columnv * $params['cellLength'])}}
                    {{$x2 = ($params['offsetX'] + $k_columnv * $params['cellLength'])}}
                    {{$y1 = ($params['offsetY'] + $k_linev * $params['cellLength'])}}
                    {{$y2 = ($params['offsetY'] + ($k_linev + 1) * $params['cellLength'])}}
                    @if($column == 1)
                        <line
                            x1="{{$x1}}" x2="{{$x2}}" y1="{{$y1}}" y2="{{$y2}}"
                            stroke-width="10"
                            stroke="yellow"
                            stroke-linecap="round"
                            rel="b_{{$k_linev + 1}}_{{$k_columnv + 1}}"
                            class="doubler"/>
                    @endif
                @endforeach
            @endforeach
            <!--horizont lines-->
            @foreach($params['accessible_way']['horizont'] as $k_line => $line)
                @foreach($line as $k_column => $column)
                    {{$x1 = ($params['offsetX'] + $k_column * $params['cellLength'])}}
                    {{$x2 = ($params['offsetX'] + ($k_column + 1) * $params['cellLength'])}}
                    {{$y1 = ($params['offsetY'] + $k_line * $params['cellLength'])}}
                    {{$y2 = ($params['offsetY'] + $k_line * $params['cellLength'])}}
                    @if($column == 1)
                        <line
                            x1="{{$x1}}" x2="{{$x2}}" y1="{{$y1}}" y2="{{$y2}}"
                            stroke-width="10"
                            stroke="yellow"
                            stroke-linecap="round"
                            rel="a_{{$k_line + 1}}_{{$k_column + 1}}"
                            class="doubler"/>
                    @endif
                @endforeach
            @endforeach

            <!--особенности-->
            <!--наверное нужно выделить в отдельный шаблон или функцию-->
            @foreach($params['specials']['cellMatrix'] as $kLine => $line)
                @foreach($line as $kColumn => $column)
                    @if($column === LABYRINTH_WHITE_CELL_POINT)
                        <circle
                            cx="{{($params['offsetX'] + $kColumn * $params['cellLength']) + $params['cellLength'] / 2}}"
                            cy="{{($params['offsetY'] + $kLine * $params['cellLength']) + $params['cellLength'] / 2}}"
                            r="{{$params['cellLength'] / 5}}"
                            style="stroke: black; fill: white;" />
                    @endif
                    @if($column === LABYRINTH_BLACK_CELL_POINT)
                        <circle
                            cx="{{($params['offsetX'] + $kColumn * $params['cellLength']) + $params['cellLength'] / 2}}"
                            cy="{{($params['offsetY'] + $kLine * $params['cellLength']) + $params['cellLength'] / 2}}"
                            r="{{$params['cellLength'] / 5}}"
                            style="stroke: black; fill: black;" />
                    @endif
                    @if($column === LABYRINTH_SIDE1_CELL_POINT)
                        {{$x1 = $params['offsetX'] + ($params['cellLength'] / 2) + (($kLine) * $params['cellLength'])}}
                        {{$x2 = $params['offsetX'] + ($params['cellLength'] / 2) + (($kLine) * $params['cellLength'])}}
                        {{$y1 = ($params['offsetY'] + ($kColumn) * $params['cellLength']) + $params['cellLength'] / 5}}
                        {{$y2 = ($params['offsetY'] + ($kColumn + 1) * $params['cellLength']) - $params['cellLength'] / 5}}
                        <line
                            x1="{{$x1}}" x2="{{$x2}}" y1="{{$y1}}" y2="{{$y2}}"
                            stroke-width="5"
                            stroke="Coral"
                            stroke-linecap="round"/>
                    @endif
                    @if($column === LABYRINTH_SIDE2_CELL_POINT)
                        {{$x1 = $params['offsetX'] + ($params['cellLength'] / 2) + (($kLine) * $params['cellLength'])}}
                        {{$x2 = $params['offsetX'] + ($params['cellLength'] / 2) + (($kLine) * $params['cellLength'])}}
                        {{$y1 = ($params['offsetY'] + ($kColumn) * $params['cellLength']) + $params['cellLength'] / 5}}
                        {{$y2 = ($params['offsetY'] + ($kColumn + 1) * $params['cellLength']) - $params['cellLength'] / 5}}
                        <line
                            x1="{{$x1-$params['cellLength'] / 10}}" x2="{{$x2 - $params['cellLength'] / 10}}" y1="{{$y1}}" y2="{{$y2}}"
                            stroke-width="5"
                            stroke="Coral"
                            stroke-linecap="round"/>
                        <line
                            x1="{{$x1+$params['cellLength'] / 10}}" x2="{{$x2 + $params['cellLength'] / 10}}" y1="{{$y1}}" y2="{{$y2}}"
                            stroke-width="5"
                            stroke="Coral"
                            stroke-linecap="round"/>
                    @endif
                    @if($column === LABYRINTH_SIDE3_CELL_POINT)
                        {{$x1 = $params['offsetX'] + ($params['cellLength'] / 2) + (($kLine) * $params['cellLength'])}}
                        {{$x2 = $params['offsetX'] + ($params['cellLength'] / 2) + (($kLine) * $params['cellLength'])}}
                        {{$y1 = ($params['offsetY'] + ($kColumn) * $params['cellLength']) + $params['cellLength'] / 5}}
                        {{$y2 = ($params['offsetY'] + ($kColumn + 1) * $params['cellLength']) - $params['cellLength'] / 5}}
                        <line
                            x1="{{$x1-$params['cellLength'] / 7}}"
                            x2="{{$x2-$params['cellLength'] / 7}}"
                            y1="{{$y1}}"
                            y2="{{$y2}}"
                            stroke-width="5"
                            stroke="Coral"
                            stroke-linecap="round"/>
                        <line
                            x1="{{$x1}}" x2="{{$x2}}" y1="{{$y1}}" y2="{{$y2}}"
                            stroke-width="5"
                            stroke="Coral"
                            stroke-linecap="round"/>
                        <line
                            x1="{{$x1+$params['cellLength'] / 7}}" x2="{{$x2+$params['cellLength'] / 7}}" y1="{{$y1}}" y2="{{$y2}}"
                            stroke-width="5"
                            stroke="Coral"
                            stroke-linecap="round"/>
                    @endif
                @endforeach
            @endforeach

            <!-- Точки входа -->
            @foreach($params['specials']['start'] as $k_start => $start)
                <circle
                    class="start"
                    cx="{{($params['offsetX'] + ($start['n'] - 1) * $params['cellLength'])}}"
                    cy="{{($params['offsetY'] + ($start['m'] - 1) * $params['cellLength'])}}"
                    r="20"
                    id="s_{{$start['m']}}_{{$start['n']}}"
                    style="stroke: grey; fill: grey; cursor: pointer" />
                 <circle
                    class="doubler"
                    cx="{{($params['offsetX'] + ($start['n'] - 1) * $params['cellLength'])}}"
                    cy="{{($params['offsetY'] + ($start['m'] - 1) * $params['cellLength'])}}"
                    r="20"
                    rel="s_{{$start['m']}}_{{$start['n']}}"
                    style="stroke: grey; fill: yellow; cursor: pointer" />
            @endforeach
        </svg>
    </div>
@endsection

@section('footer')
    <script src='/js/labyrinth.js?{{time()}}'></script>
@endsection
