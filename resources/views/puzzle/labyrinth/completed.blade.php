@extends('layouts.common')
@section('header')
@endsection

@section('content')
    @foreach($params['labyrinths'] as $labyrinth)
        @include('puzzle.labyrinth.minipreview', ['labyrinth' => $labyrinth])
    @endforeach
@endsection

@section('footer')
    
@endsection