@extends('layouts.common')

@section('header')
@endsection

@section('content')
    <div class="inner_puzzle" style="background-image: url('/screensaver/?region={{$params['region_id']}}&t={{time()}}');">
        @yield('inner_puzzle')
    </div>
@endsection