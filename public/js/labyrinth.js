$(function() {
    var labyrinth = {
        shiny: false,
        noActiveColor: 'grey',
        activeColor: 'yellow',
        shinyColor: '#00FA9A',
        lastShinyColor: 'yellow',
        m: $('input[name=m]').val(),
        n: $('input[name=n]').val(),
        id: $('input[name=id]').val(),
        types: $('input[name=types]').val(),
        specials: $('input[name=specials]').val(),
        start: {},
        end: {},
	ribs : {max_x: 0, max_y: 0, min_x: 100000, min_y: 100000,
            min_y_rib: 'b_1_1', min_x_rib: 'a_1_1'},
        allAccessRibs : [],
	allAccessRibsCurrent: [],
        labyrinth_process: 0,
	allAccessPoints : {},
	allCompleteRibs : [],
	allCompletePoints: [],
        offsetX: $('#puzzle_draw').position().left + 25,
        offsetY: $('#puzzle_draw').position().top + 25,
	// пересчитываем все доступные рёбра
	reCountAccessRibs : function() {
            if (labyrinth.allCompleteRibs.length === 0) {
		var p = labyrinth.allCompletePoints[0];
		var point = '';
                for (var key in p) {
                    point = key;
                    break;
                }
		labyrinth.allAccessRibsCurrent = getNeighboursRibsFromPoint(point);
            } else {
		if (labyrinth.allCompleteRibs.length === 1) {
                    var completeAccessRibs = labyrinth.allCompleteRibs.slice();
                    completeAccessRibs.pop();
                    var tmp = arrayDifference(getNeighboursRibsFromRib(labyrinth.allCompleteRibs[labyrinth.allCompleteRibs.length - 1]), completeAccessRibs);
                    labyrinth.allAccessRibsCurrent = arrayDifference(tmp, labyrinth.getNeighboursOfCompleteRibs());
		} else {	
                    var tmpRibs = getNeighboursRibsFromRib(labyrinth.allCompleteRibs[labyrinth.allCompleteRibs.length - 1]);
                    // тут нужно будет исключить всех соседей пройденных рёбер
                    if (labyrinth.allCompleteRibs.length > 3) {
                        for (var i = labyrinth.allCompleteRibs.length - 4; i >= 0; i--) {
                            tmpRibs = arrayDifference(tmpRibs, getNeighboursRibsFromRib(labyrinth.allCompleteRibs[i]));
			}
                    }
                    labyrinth.allAccessRibsCurrent = tmpRibs;
		}
            }
	},
	// получаем всех соседей пройденных рёбер (кроме последнего)
	getNeighboursOfCompleteRibs : function() {
            var result = [];
            if (labyrinth.allCompleteRibs.length > 2) {
		for (var i = labyrinth.allCompleteRibs.length - 2; i >= 0; i--) {
                    var curComplete = getNeighboursRibsFromRib(labyrinth.allCompleteRibs[i]);
                    for (var j = 0; j < curComplete.length; j++) {
			if (!inArray(curComplete[j], result)) {
                            result.push(curComplete[j]);
			}
                    }
		}
            }
            return result;
	},
        // определяем, можно ли нажать на "решить лабиринт"
        detectFinish: function (X, Y) {
            if (this.allCompleteRibs.length > 0) {
                var lastRib = this.allCompleteRibs[this.allCompleteRibs.length - 1];
                if ($('.exit').length > 1) {
                    var minLength = 1000000;
                    var exitPoint = {};
                    $('circle.start').each(function(){
                        var temp = getDistanse([X, Y], [$(this).attr('x1'), $(this).attr('y1')]);
                        if(temp < minLength){
                            exitPoint = $(this);
                            minLength = temp;
                        }
                    });
                } else {
                    var exitPoint = $('.exit');
                }
             
                var distance = getDistanse([X, Y], [exitPoint.attr('x1'), exitPoint.attr('y1')]);
                if (distance < 30 && inArray(lastRib, getNeighboursRibsFromPoint(exitPoint.attr('id')))) {
                    labyrinth.shiny = true;
                } else {
                    labyrinth.shiny = false;
                }
            }
        }
    };
    
    $('line.rib').each(function () {
        var id = $(this).attr('id');
        if (!id) {
            return false;
        }
	labyrinth.ribs[id] = {
            x1:$(this).attr('x1'), 
            x2: $(this).attr('x2'), 
            y1: $(this).attr('y1'), 
            y2: $(this).attr('y2')
        };
	if (id.indexOf('a_') !== -1) {	
            if ($(this).attr('y2') >= labyrinth.ribs.max_y) {
		labyrinth.ribs.max_y = $(this).attr('y2');
		labyrinth.ribs.max_y_rib = id;
            }
            if ($(this).attr('y2') <= labyrinth.ribs.min_y) {
		labyrinth.ribs.min_y = $(this).attr('y2');
		labyrinth.ribs.min_y_rib = id;
            }
	}
	if (id.indexOf('b_') !== -1) {
            if ($(this).attr('x2') >= labyrinth.ribs.max_x) {
		labyrinth.ribs.max_x = $(this).attr('x2');
		labyrinth.ribs.max_x_rib = id;
            }
            if ($(this).attr('x1') <= labyrinth.ribs.min_x) {
		labyrinth.ribs.min_x = $(this).attr('x1');
		labyrinth.ribs.min_x_rib = id;
            }
	}
	if (id != 'current_rib') {
            labyrinth.allAccessRibs.push(id);
	}
	// ещё нужно сохранять все точки лабиринта
	var point = dotFromRib(id);
	for (var i = 0; i <= 1; i++) {
            for (var j in point[i]) {
                if (!labyrinth.allAccessPoints[j]) {
                    labyrinth.allAccessPoints[j] = point[i][j];	
		}
            }
	}
    });
   
    /**
     * Инициализация лабиринта
     */
    function initLabyrinth(labyrinth) {
        labyrinth.shiny = false;
        labyrinth.lastShinyColor = labyrinth.activeColor;
        labyrinth.labyrinth_process = 0;
        labyrinth.allCompleteRibs = [];
        labyrinth.allCompletePoints = [];
            
        $('#current_rib').css({stroke: labyrinth.activeColor, transition: "1s"}); 
        var startId = $('.current_start').attr('id');
        $('circle[rel='+startId+']').css({fill: labyrinth.activeColor, transition: "1s"});
        for (var i = 0; i < labyrinth.allAccessRibs.length - 1; i++) {
            $('[rel='+labyrinth.allAccessRibs[i]+']').css({stroke: labyrinth.activeColor, transition: "1s"});
	}
        
        $('.doubler').hide();
        $('circle.start').attr('class', 'start');
        $('#current_rib').hide();
        
        console.log('init_labyrinth');
    }
    
    /**
     * Находит координаты курсора внутри элемента
     */
    function getCoords(pos, e){
        var elem_left = pos.left;
        var elem_top = pos.top;
        // положение курсора внутри элемента
        e.Xinner = e.pageX - elem_left;
        e.Yinner = e.pageY - elem_top;
        return e;
    }
    
    /**
     * Получение расстояния между двумя точками
     */
    function getDistanse(point1, point2){
        var dx = (point2[0] - point1[0]) * (point2[0] - point1[0]);
        var dy = (point2[1] - point1[1]) * (point2[1] - point1[1]);
        var sqrt = Math.sqrt(dx + dy);
        return sqrt;
    }
    
    // получить точки данного ребра
    function dotFromRib(id) {
	var arr = id.split("_");
	var x1 = $('#'+id).attr('x1');
	var x2 = $('#'+id).attr('x2');
	var y1 = $('#'+id).attr('y1');
	var y2 = $('#'+id).attr('y2');
	var key1 = '' ,key2 = '';
	var obj1 = {};
	var obj2 = {};

	if (arr[0] == 'a') {
            key1 = "" + arr[1] + '_' + arr[2];
            key2 = "" + arr[1] + '_' + (arr[2] * 1 + 1);
            obj1[key1] = [x1, y1];
            obj2[key2] = [x2, y2];	
	} else {
            key1 = "" + arr[1] + '_' + arr[2];
            key2 = "" + (arr[1] * 1 + 1) + '_' + arr[2];
            obj1[key1] = [x1, y1];
            obj2[key2] = [x2, y2];
	}
	return [obj1, obj2];
    }
	
    // нахождение ближайшей точки по координатам в заданном лабиринте    
    function nearestRib(x, y, lab) {
	var min_x = min_y = 100000;
	var min_x_rib = min_y_rib = 0;
	var ribs = lab.ribs;
    	for (p in ribs) {
            // если x или y внутри лабиринта
            if ((x > ribs[p].x1 && x < ribs[p].x2) || (y > ribs[p].y1 && y < ribs[p].y2)) {
                // a
                if (p.indexOf('a_') !== -1) {
                    if (Math.abs(y - ribs[p].y1) <= min_y) {
			min_y = Math.abs(y - labyrinth.ribs[p].y1);
			min_y_rib = p;
                    }
		}
		// b
		if (p.indexOf('b_') !== -1) {
                    if (Math.abs(x - ribs[p].x1) <= min_x) {
                        min_x = Math.abs(x - labyrinth.ribs[p].x1);
			min_x_rib = p;
                    }
		}
            } 
	}

	// если мышь оказалась где-то за гранью
	if (min_x_rib === 0 && min_y_rib === 0) {
            // правый нижний угол
            if (x >= ribs.max_x && y >= ribs.max_y) {
		min_x = Math.abs(x - ribs.max_x);
		min_y = Math.abs(y - ribs.max_y);
			
		min_x_rib = ribs.max_x_rib;
		min_y_rib = ribs.max_y_rib;
            }
            // правый верхний угол
            if (x >= ribs.max_x && y <= ribs.min_y) {
		min_x = Math.abs(x - ribs.max_x);
		min_y = Math.abs(y - ribs.min_y);

		min_x_rib = 'a_1_' + ribs.n;
		min_y_rib = 'b_1_' + (ribs.n * 1 + 1);
            }
            // левый верхний угол
            if (x <= ribs.min_x && y <= ribs.min_y) {
		min_x = Math.abs(x - ribs.min_x);
		min_y = Math.abs(y - ribs.min_y);
			
		min_x_rib = ribs.min_x_rib;
		min_y_rib = ribs.min_y_rib;
            }
            // левый нижний угол
            if (x <= ribs.min_x && y >= ribs.max_y) {
		min_x = Math.abs(x - ribs.min_x);
		min_y = Math.abs(y - ribs.max_y);
			
		min_x_rib = ribs.min_x_rib;
		min_y_rib = ribs.max_y_rib;
			
		min_x_rib = 'a_' + (ribs.m * 1 + 1) + '_1';
		min_y_rib = 'b_' + lab.m + '_1';
            }
	}

	min_x_rib = min_x_rib ? min_x_rib : ribs.min_x_rib;
	min_y_rib = min_y_rib ? min_y_rib : ribs.min_y_rib;               
                
	return min_x < min_y ? min_x_rib : min_y_rib;
    }
	
    // для точки доступно ли данное ребро
    function accessRibsFromPoint(r, point) {
	for (var k in point) {
            curPointArray = k.split("_");
            break;
	}
	if (!inArray(r, labyrinth.allAccessRibs)) {
            return false;
	}
	if (r === '' + 'a_' + k[0] + '_' + (k[1] - 1)
            || r === '' + 'a_' + k[0] + '_' + k[1]
            || r === '' + 'b_' + k[0] + '_' + k[1]
            || r === '' + 'b_' + (k[0] - 1) + '_' + k[1]
	) {
            return true;
	}
	return false;
    }

    // рисование линии в браузере
    function drawLine(pointTo, labyrinth) {
	$('.doubler').hide();
	// рисуем стартовую линию
	var startId = $('.current_start').attr('id');
	$('circle[rel='+startId+']').show();
	// рисуем все рёбра, которые уже пройдены
        var to = labyrinth.allCompleteRibs.length - 1;
        if (labyrinth.shiny) {
           to++; 
        }
	for (var i = 0; i < to; i++) {
            $('[rel='+labyrinth.allCompleteRibs[i]+']').show();
	}	
	
        // рисуем неполную линию pointTo
	var halfLine = $('#current_rib');
	halfLine.attr('x1',pointTo.x1).attr('x2', pointTo.x2).attr('y1', pointTo.y1).attr('y2', pointTo.y2);	
    }
	
    function inArray(needle, arr) {
	for (var i = 0; i < arr.length; i++) {
            if (arr[i] == needle) {
		return true;
            }
	}
	return false;
    }
	
    // функция нахождения разности массивов
    function arrayDifference(A, B) {
        var result = [];
	for (var i = 0; i < A.length; i++) {
            if (inArray(A[i], B)) {
            	continue;
            } else {
                result.push(A[i]);
            }
        }
	return result;
    }

    // для точки возвращает все доступные рёбра
    function getAccessRibsFromPoint(point) {
	for (var k in point) {
            var curPointArray = k.split("_");
            break;
	}
		
	var allRibs = [
            'a_' + k[0] + '_' + (k[1] - 1), 
            'a_' + k[0] + '_' + k[1], 
            'b_' + k[0] + '_' + k[1], 
            'b_' + (k[0] - 1) + '_' + k[1]
        ];
	var result = [];
	for (var i = 0; i < allRibs.length; i++) {
            if (inArray(allRibs[i], labyrinth.allAccessRibs)) {
		result.push(allRibs[i]);
            }
	}
	return result;
    }
	
    // смотрим, являются ли два ребра соседними
    function isNeighbourRibs(curRib, lastRib) {
	if (!lastRib) {
            return false;
	} 
	var lastRibAr = lastRib.split("_");
	lastRibAr[1] = lastRibAr[1] * 1;
	lastRibAr[2] = lastRibAr[2] * 1;
		
	if (lastRibAr[0] == 'a') {
            var allRibsFromLast = [
                'a_' + lastRibAr[1] + '_' + (lastRibAr[2] - 1),
                'a_' + lastRibAr[1] + '_' + (lastRibAr[2] + 1),
                'b_' + (lastRibAr[1] - 1) + '_' + lastRibAr[2],
                'b_' + (lastRibAr[1] - 1) + '_' + (lastRibAr[2] + 1),
                'b_' + lastRibAr[1] + '_' + lastRibAr[2],
                'b_' + lastRibAr[1] + '_' + (lastRibAr[2] + 1),
            ];
        } else {
            var allRibsFromLast = [
		'b_' + (lastRibAr[1] - 1) + '_' + lastRibAr[2],
		'b_' + (lastRibAr[1] + 1) + '_' + lastRibAr[2],
                'a_' + lastRibAr[1] + '_' + (lastRibAr[2] - 1),
		'a_' + lastRibAr[1] + '_' + lastRibAr[2],
		'a_' + (lastRibAr[1] + 1) + '_' + (lastRibAr[2] - 1),
            	'a_' + (lastRibAr[1] + 1) + '_' + lastRibAr[2],
            ];
	}
	if (inArray(curRib, allRibsFromLast)) {
            return true;
	}
	return false;
    }
	
    // получаем список соcедей-рёбер данного ребра
    function getNeighboursRibsFromRib(r){
	var ribAr = r.split("_");
	ribAr[1] = ribAr[1] * 1;
	ribAr[2] = ribAr[2] * 1;
	var allRibs = [];
	if (ribAr[0] == 'a') {
            allRibs = [
                'a_' + ribAr[1] + '_' + (ribAr[2] - 1),
		'a_' + ribAr[1] + '_' + (ribAr[2] + 1),
		'b_' + (ribAr[1] - 1) + '_' + ribAr[2],
		'b_' + (ribAr[1] - 1) + '_' + (ribAr[2] + 1),
		'b_' + ribAr[1] + '_' + ribAr[2],
		'b_' + ribAr[1] + '_' + (ribAr[2] + 1),
            ];
	} else {
            allRibs = [
		'b_' + (ribAr[1] - 1) + '_' + ribAr[2],
		'b_' + (ribAr[1] + 1) + '_' + ribAr[2],
		'a_' + ribAr[1] + '_' + (ribAr[2] - 1),
		'a_' + ribAr[1] + '_' + ribAr[2],
		'a_' + (ribAr[1] + 1) + '_' + (ribAr[2] - 1),
		'a_' + (ribAr[1] + 1) + '_' + ribAr[2],
            ];
	}
	var result = [];
        for (var i = 0; i < allRibs.length; i++) {
            if (inArray(allRibs[i], labyrinth.allAccessRibs)) {
		result.push(allRibs[i]);
            }
	}
	return result;
    }
	
    // получаем список соcедей-рёбер данной точки
    function getNeighboursRibsFromPoint(point) {
	var pointAr = point.split("_");
	pointAr[0] = pointAr[0] * 1;
	pointAr[1] = pointAr[1] * 1;
	var allRibs = [
            'a_' + pointAr[0] + '_' + pointAr[1],
            'a_' + pointAr[0] + '_' + (pointAr[1] - 1),
            'b_' + (pointAr[0] - 1) + '_' + pointAr[1],
            'b_' + pointAr[0] + '_' + pointAr[1]
	];
	var result = [];
	for (var i = 0; i < allRibs.length; i++) {
            if (inArray(allRibs[i], labyrinth.allAccessRibs)) {
                result.push(allRibs[i]);
            }
	}
        
	return result;
    }
	
    // смотрим, является ли точка общей для двух ребёр
    function isCommonDotToRibs(dot, rib1, rib2) {
	for (var k in dot) {
            var curPointArray = k.split("_");
            break;
	}
	var rib1Ar = rib1.split("_");
	var rib2Ar = rib2.split("_");
	
	var flag = false;
		
	if (rib1Ar[0] == 'a') {
            if (rib2Ar[0] == 'b') {
		if (
                    ((rib1Ar[1] == rib2Ar[1]) && (curPointArray[0] == rib1Ar[1])) || 
                    (((rib1Ar[1] - 1) == rib2Ar[1]) && curPointArray[0] == rib1Ar[1])
                ) {
                    flag = true;
		}
            }
	} else {
            if (rib2Ar[0] == 'a') {
		if (
                    ((rib1Ar[2] == rib2Ar[2]) && (curPointArray[1] == rib1Ar[2])) || 
                    (((rib1Ar[2] - 1) == rib2Ar[2]) && (curPointArray[1] == rib1Ar[2]))
                ) {
                    flag = true;
		}
            } 
	}

        return flag;
    }
	
    // получаем общую точку для 2-х рёбер
    function getCommonDotFromRibs(rib1, rib2) {
	var d1 = dotFromRib(rib1);
	var d2 = dotFromRib(rib2);
	if ((JSON.stringify(d1[0]) == JSON.stringify(d2[0])) || 
            (JSON.stringify(d1[0]) == JSON.stringify(d2[1]))
        ) {
            return d1[0];
	}
	if ((JSON.stringify(d1[1]) == JSON.stringify(d2[0])) || 
            (JSON.stringify(d1[1]) == JSON.stringify(d2[1]))
        ) {
            return d1[1];
	}
	return false;
    }
    
    // получаем объект недорисованного ребра
    function getPointTo(curRib, point, X, Y) {
        var completeAccessRibs = labyrinth.allAccessRibsCurrent.slice();
        completeAccessRibs.push(labyrinth.allCompleteRibs[labyrinth.allCompleteRibs.length - 1]);
        if (!inArray(curRib, completeAccessRibs)) {
            return {};
        }
        for (var k in point) {
            var curPointArray = k.split("_");
            break;
	}

        var rib = $('#'+curRib)
        var ribInfo = curRib.split('_');
        var pointTo = {};
        
        if (ribInfo[0] == 'a') {
            pointTo['y1'] = rib.attr('y1');
            pointTo['y2'] = rib.attr('y2');
            
            if (point[k][0] == rib.attr('x1') && point[k][1] == rib.attr('y1')) {
                pointTo['x1'] = point[k][0];
                pointTo['x2'] = X > rib.attr('x2') || X < rib.attr('x1') ? rib.attr('x2') : X;
            } else {
                pointTo['x1'] = X < rib.attr('x1') || X > rib.attr('x2') ? rib.attr('x1') : X;
                pointTo['x2'] = point[k][0];
            }
        } else {
            pointTo['x1'] = rib.attr('x1');
            pointTo['x2'] = rib.attr('x2');

            if (point[k][0] == rib.attr('x1') && point[k][1] == rib.attr('y1')) {
                pointTo['y1'] = point[k][1];
                pointTo['y2'] = Y > rib.attr('y2') || Y < rib.attr('y1') ? rib.attr('y2') : Y;
            } else {
                pointTo['y1'] = Y < rib.attr('y1') || Y > rib.attr('y2') ? rib.attr('y1') : Y;
                pointTo['y2'] = point[k][1];
            }
        }
        
        if (X > labyrinth.ribs.max_x) {
            pointTo['x1'] = labyrinth.ribs.max_x;
            pointTo['x2'] = labyrinth.ribs.max_x;
        }
        // возможно аналогично понадобится, если будет лишняя прорисовка, 
        // когда мышь за границей лабиринта
        
        return pointTo;
    }
    
    // Функция для определения координат указателя мыши
    function defPosition(event) {
        var x = y = 0;
        if (document.attachEvent != null) { // Internet Explorer & Opera
            x = window.event.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
            y = window.event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
        } else if (!document.attachEvent && document.addEventListener) { // Gecko
            x = event.clientX + window.scrollX;
            y = event.clientY + window.scrollY;
        } else {
            // Do nothing
        }
        return {x:x, y:y};
    }
        
    $('#content div.puzzle_placeholder').bind('mousedown', function(e) {
        // если нажали левую кнопку мыши
        if (e.button === 0) {
            //функция перехода на ближайший старт
            if (labyrinth.labyrinth_process !== 1) {
                labyrinth.labyrinth_process = 1; 
                labyrinth.ribs.min_x = $('#'+labyrinth.ribs.min_x_rib).attr('x1');
                labyrinth.ribs.min_y = $('#'+labyrinth.ribs.min_y_rib).attr('y1');

                var choosen = {};
            
                if ($('circle.start').length > 1) {
                    var pos = $(this).offset();
                    e = getCoords(pos, e);
                    var click_point = [e.Xinner, e.Yinner];
                    // находим координаты каждого старта
                    var current_start = {};
                    var distances = [];
                    var i = 0;
                    var min = 100000000;
                    $('circle.start').each(function(){
                        current_start = $(this);
                        var offsetX = $(this).parent().position().left;
                        var offsetY = $(this).parent().position().top;
                        var current_coords = $(this).position();
                        
                        current_coords = [
                            current_coords.left - offsetX, 
                            current_coords.top - offsetY
                        ];
                        
                        var temp = getDistanse(click_point, current_coords);
                        if (temp < min) {
                            choosen = $(this);
                            min = temp;
                        }
                    });
                } else {   
                    choosen = $('circle.start');
                }
                
                choosen.attr('class', choosen.attr('class') + ' current_start');
            
                var startId = choosen.attr('id');
                $('circle[rel='+startId+']').show();
            
                var startPoint = choosen.attr('id').split('_');
                var endPoint = $('.exit');
                labyrinth.ribs.m = labyrinth.m;
                labyrinth.ribs.n = labyrinth.n;
                var startKey = startPoint[1] + '_' + startPoint[2];
                // тут данные буту браться из формы, в котором находится лабиринт
                labyrinth.start[startKey] = [
                    choosen.attr('cx'), choosen.attr('cy')
                ];
                labyrinth.end[endPoint.attr('id')] = [
                    endPoint.attr('x1'), endPoint.attr('y1')
                ]; 
                // конец данных из бэкэнда
                labyrinth.allCompletePoints.push(labyrinth.start);
                labyrinth.reCountAccessRibs();
                // прорисовка полуребра
                $('#current_rib').attr('x1',choosen.attr('cx')).attr('x2',choosen.attr('cx'));
                $('#current_rib').attr('y1',choosen.attr('cy')).attr('y2',choosen.attr('cy'));
                $('#current_rib').show();
            } else {
                if (labyrinth.shiny) {
                    var sendObject = {
                        m: labyrinth.m, 
                        n: labyrinth.n,
                        path: labyrinth.allCompleteRibs.join(":"),
                        types: labyrinth.types,
                        specials: labyrinth.specials,
                        id: labyrinth.id
                    };
                    sendObject['_token'] = $('input[name="_token"]').val();
                    $.post("/labyrinth/resolve", sendObject, function(data) {
                        data = JSON.parse(data);
                        if (data.result === true) {
                            alert('Верно!');
                            var path = location.href.split('/');
                            path.pop();
                            location.href = path.join('/'); 
                        } else {
                            alert('Неверно!');
                        }
                    });
                }
            }
        }
    });		
    
    $('#puzzle_draw').bind('mousemove', function(e) {
        if (!labyrinth.labyrinth_process) {
            return false;
        }
        
        var X = e.pageX - labyrinth.offsetX; // положения по оси X
	var Y = e.pageY - labyrinth.offsetY; // положения по оси Y
        
        labyrinth.detectFinish(X, Y);
        
	// получили ближайшие ребро и точку к курсору мыши
	var r = nearestRib(X, Y, labyrinth);
        
        var lastPoint = labyrinth.allCompletePoints[labyrinth.allCompletePoints.length - 1];
	
	/** 
            проверяем, есть ли ближайшее ребро в списке доступных рёбер. 
            Если есть, тогда производим стандартные действия над расчётом и формированием рёбер.
            Если нет. то просто производим прорисовку по имеющимся данным.
	*/
	if (inArray(r, labyrinth.allAccessRibsCurrent) 
            || r == labyrinth.allCompleteRibs[labyrinth.allCompleteRibs.length - 1]) 
        {
            if (labyrinth.allCompleteRibs.length === 0) {
		labyrinth.allCompleteRibs.push(r);
            }
		
            var lastRib = labyrinth.allCompleteRibs[labyrinth.allCompleteRibs.length - 1];
            
            if (r != lastRib) {
		if (isNeighbourRibs(r, lastRib)) {
                    var commonDot = getCommonDotFromRibs(r, lastRib);
                    if (isCommonDotToRibs(lastPoint, r, lastRib) && 
                        labyrinth.allCompletePoints.length == 1) {
                        // TODO: проверить, верно ли исправил баг с тем, чтобы не сравнивать 
			labyrinth.allCompleteRibs.pop();
			labyrinth.allCompleteRibs.push(r);
                    } else {
                        if (labyrinth.allCompleteRibs.length > 2) {
                            if (!inArray(r, labyrinth.allCompleteRibs)) {
                                if (isNeighbourRibs(r, labyrinth.allCompleteRibs[labyrinth.allCompleteRibs.length - 2])
                                    && isNeighbourRibs(lastRib, labyrinth.allCompleteRibs[labyrinth.allCompleteRibs.length - 2])
				) {
                                    labyrinth.allCompleteRibs.pop();
                                    labyrinth.allCompleteRibs.push(r);
				} else {
                                    labyrinth.allCompleteRibs.push(r);
                                    labyrinth.allCompletePoints.push(commonDot);
				}
                            } else {
				if (labyrinth.allCompleteRibs[labyrinth.allCompleteRibs.length - 2] 
                                    && r == labyrinth.allCompleteRibs[labyrinth.allCompleteRibs.length - 2]) 
                                {
                                    labyrinth.allCompleteRibs.pop();
                                    if (labyrinth.allCompletePoints.length > 1) {
                                        labyrinth.allCompletePoints.pop();
                                    }
				}
                            }
                        } else {
                           if (!inArray(r, labyrinth.allCompleteRibs)) { 
                                if (labyrinth.allCompleteRibs.length == 2 && 
                                    isNeighbourRibs(lastRib, labyrinth.allCompleteRibs[0]) &&
                                    isNeighbourRibs(r, labyrinth.allCompleteRibs[0])
                                ) {
                                    labyrinth.allCompleteRibs.pop();
                                    labyrinth.allCompleteRibs.push(r);
                                } else {
                                    labyrinth.allCompleteRibs.push(r);
                                    labyrinth.allCompletePoints.push(commonDot);
                                }
                            } else {
				if (labyrinth.allCompleteRibs.length == 2) {
                                    labyrinth.allCompleteRibs.pop();
                                    if (labyrinth.allCompletePoints.length > 1) {
                                        labyrinth.allCompletePoints.pop();
                                    }
				}
                            }
			}
                    }
		} else if (!lastRib) {
                    labyrinth.allCompleteRibs.push(r);
		} else {
			
		}
            } else {
                           
            }
            // пересчитываем доступные рёбра
            labyrinth.reCountAccessRibs();
	} else {
	
	}
        //отрисовать pointTo в случае, когда курсор выходи за границу
        var pointTo = getPointTo(r, lastPoint, X, Y);
	drawLine(pointTo, labyrinth);
    });
    
    // если нажали правуюю кнопку мыши
    $('.puzzle_placeholder').bind('contextmenu', function(e) {
        var menu = $('#contextMenuLabyrinth');
        menu.show();
        menu.css({top: defPosition(e).y + "px", left: defPosition(e).x + "px"});
        return false;
    });
    
    $('.js-labyrinth-init').bind('click', function(e){
        e.preventDefault();
        initLabyrinth(labyrinth);
        $('#contextMenuLabyrinth').hide();
    });
    $('.exit').bind('mouseover', function(){
        if (labyrinth.shiny) {
            var startId = $('.current_start').attr('id');
            var color = $('circle[rel='+startId+']').css('fill');
        
            $(this).css({stroke: color});
            $(this).attr('class', 'exit active_exit');
        }
    });
    $('.exit').bind('mouseout', function(){
        $(this).css({stroke:labyrinth.noActiveColor});
        $(this).attr('class', 'exit');
    });
    
    setInterval(function() {
        if (labyrinth.labyrinth_process && labyrinth.shiny === true) {
            var color = labyrinth.lastShinyColor == labyrinth.activeColor ? 
                labyrinth.shinyColor : labyrinth.activeColor;
            labyrinth.lastShinyColor = color;
        } else {
            var color = labyrinth.activeColor;
        }
        
        $('#current_rib').css({stroke: color, transition: "1s"}); 
        for (var i = 0; i < labyrinth.allCompleteRibs.length; i++) {
            $('[rel='+labyrinth.allCompleteRibs[i]+']').css({stroke: color, transition: "1s"});
        }
        var startId = $('.current_start').attr('id');
        $('circle[rel='+startId+']').css({fill: color, transition: "1s"});
        
        $('.active_exit').css({stroke: color, transition: "1s"});
    }, 1000);
});