<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Traits\Helper;

/**
 * Class AppServiceProvider
 *
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    use Helper;

    /**
     * @var array $pages
     */
    private $pages = [
        [
            'slug' => 'home',
            'status' => 1,
            'title' => 'Домашняя',
            'autorized' => [],
            'common' => 1
        ], [
            'slug' => 'about',
            'status' => 1,
            'title' => 'О проекте',
            'autorized' => [],
            'common' => 1
        ], [
            'slug' => 'journeys',
            'status' => 1,
            'title' => 'Головоломки',
            'autorized' => 1,
            'common' => 1
        ]
    ];

    /**
     * @return void
     */
    public function boot()
    {
        $helper = [];
        $helper['pages'] = $this->pages;

        view()->share('helper', $helper);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
