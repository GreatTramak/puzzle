<?php

namespace App\Http\Traits;

/**
 * Trait Helper
 *
 * @package App\Http\Traits
 */
trait Helper
{
    /**
     * @brief Rebuild object data to array
     *
     * @param mixed $obj
     *
     * @return mixed
     */
    public function objectToArray($obj)
    {
        if (is_object($obj) && empty($obj)) {
            return [];
        }

        $new = [];
        if (is_object($obj)) {
            $obj = (array)$obj;
        }
        if (is_array($obj)) {
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else if (is_scalar($obj)) {
            $new = $obj;
        }

        return $new;
    }
}
