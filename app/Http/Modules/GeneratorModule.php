<?php

namespace App\Http\Modules;

use App\Http\Controllers\LabyrinthController;
use App\Http\Traits\Helper;

/**
 * Class GeneratorModule
 *
 * @package App\Http\Modules
 */
class GeneratorModule
{

    use Helper;

    /**
     * @var array $allCasesMatrix
     */
    public $allCasesMatrix = [];

    /**
     * @param int $m
     * @param int $n
     * @param string $type
     *
     * @return array
     */
    public function generateByDimensions(int $m, int $n, string $type = 'h'): array
    {
        $resultArray = [];
        if ($type == 'v') {
            $n++;
        } else {
            $m++;
        }

        for ($i = 0; $i < $m; $i++) {
            for ($j = 0; $j < $n; $j++) {
                $resultArray[$i][$j] = 1;
            }
        }

        return $resultArray;
    }

    /**
     * @param array $matrix
     *
     * @return array
     */
    public function transponateMatrix(array $matrix)
    {
        $rows = count($matrix);
        $cols = count($matrix[0]);
        $Tmatrix = [];
        for ($i = 0; $i < $rows; $i++) {
            for ($j = 0; $j < $cols; $j++) {
                $Tmatrix[$j][$i] = $matrix[$i][$j];
            }
        }

        return $Tmatrix;
    }

    /**
     * @brief генерация всех вариантов точек для данной матрицы
     *
     * @param int $m
     * @param int $n
     * @param int $count
     * @param string $type
     *
     * @return array
     */
    public function generatePoints(int $m, int $n, int $count, string $type = 'start'): array
    {
        // количество точек по рёбрам равна размерность + 1
        $m++;
        $n++;

        $resultArray = [];
        $stepResultArray = [];
        $currentCount = 1;
        $point = [];
        while ($currentCount <= $count) {
            // смотрим текущее количество точек: формируем начальные, или дополняем на основе существующих
            if ($currentCount == 1) {
                for ($i = 1; $i <= $m; $i++) {
                    for ($j = 1; $j <= $n; $j++) {
                        if ($type == 'end' && (($i != $m) && ($i != 1) && ($j != 1) && ($j != $n))) {

                        } else {
                            $point = ['m' => $i, 'n' => $j];
                            $stepResultArray[$currentCount - 1][] = [$point];
                        }
                    }
                }
            } else {
                $currHandleArray = $stepResultArray[$currentCount - 2];
                foreach ($currHandleArray as $arr) {
                    for ($i = 1; $i <= $m; $i++) {
                        for ($j = 1; $j <= $n; $j++) {
                            if ($type == 'end' && (($i != $m) && ($i != 1) && ($j != 1) && ($j != $n))) {

                            } else {
                                $point = ['m' => $i, 'n' => $j];
                                if (in_array($point, $arr)) {
                                    continue;
                                } else {
                                    $t = $arr;
                                    $t[] = $point;
                                    sort($t);
                                    $stepResultArray[$currentCount - 1][] = $t;
                                }
                            }
                        }
                    }
                }
            }
            $currentCount++;
            $resultArray = array_merge($resultArray, $stepResultArray[$currentCount - 2]);
        }
        $t = [];
        foreach ($resultArray as $k => $v) {
            if (!in_array($v, $t)) {
                $t[] = $v;
            }
        }
        return $t;
    }

    /**
     * @param array $matrix
     *
     * @return array
     */
    public function generateAccessiblenessMatrix(array $matrix): array
    {
        $curHandleArray = [];
        $m = count($matrix);
        $n = count($matrix[0]);
        $resultArray[] = $matrix;
        $curMatrix = $matrix;
        $step = 0;
        while (true) {
            if ($step > 0 && !isset($curHandleArray[$step])) {
                break;
            }
            if ($step == 0) {
                for ($i = 0; $i < $m; $i++) {
                    for ($j = 0; $j < $n; $j++) {
                        $curMatrix = $matrix;
                        $curMatrix[$i][$j] = 0;
                        $curHandleArray[] = $curMatrix;
                        $resultArray[] = $curMatrix;
                    }
                }
            } else {
                $curMatrix = $curHandleArray[$step - 1];
                for ($i = 0; $i < $m; $i++) {
                    for ($j = 0; $j < $n; $j++) {
                        $t = $curMatrix;
                        if ($t[$i][$j] != 0) {
                            $t[$i][$j] = 0;
                            if (!$this->isZeroMatrix($t) && !in_array($t, $resultArray)) {
                                $resultArray[] = $t;
                                $curHandleArray[] = $t;
                            }
                        }
                    }
                }
            }
            $step++;
        }

        return $resultArray;
    }

    /**
     * @brief Проверка является ли матрица пустой
     *
     * @param array $matrix
     * @param int|string $elem
     *
     * @return bool
     */
    public function isZeroMatrix(array $matrix, $elem = 0)
    {
        foreach ($matrix as $k => $v) {
            foreach ($v as $k1 => $v1) {
                if ($v1 != $elem) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @brief Получение множества всех подмножеств одномерного массива
     *
     * @param array $specials
     *
     * @return array
     */
    public function generateSpesialsArray(array $specials): array
    {
        $returnArray = [$specials];
        $curHandleArray = [$specials];
        $allCount = pow(2, count($specials)) - 1;
        $count = 0;
        $r = [];
        while (count($r) < $allCount) {
            $tmpCurArray = [];
            foreach ($curHandleArray as $k => $v) {
                foreach ($v as $key => $value) {
                    if (!empty($v)) {
                        $t = $v;
                        unset($t[$key]);
                        $tmpCurArray[] = $t;
                        $returnArray[] = $t;
                    }
                }
            }
            $count++;
            $curHandleArray = $tmpCurArray;

            foreach ($returnArray as $v1) {
                $v1 = array_values($v1);
                if (!in_array($v1, $r) && !empty($v1)) {
                    $r[] = $v1;
                }
            }
        }

        return $r;
    }

    /**
     * @brief Рассчёт характеристик генерируемого лабиринта по типу
     *
     * @param string $spec
     * @param int $countCellsTypes
     * @param int $countRibsTypes
     * @param array $dim
     *
     * @return array
     */
    public function calculateMaxCountElementsByType(string $spec, int $countCellsTypes,
                                                    int $countRibsTypes, array $dim): array
    {
        $h = count($dim['H']);
        $h0 = count($dim['H'][0]);
        $v = count($dim['V']);
        $v0 = count($dim['V'][0]);
        $maxCells = $h0 * $v;
        $maxRibs = ($h0 * $h) + ($v0 * $v);
        $maxRibsByType = ($countRibsTypes > 0) ? floor($maxRibs / $countRibsTypes) : $maxRibs;
        $maxCellsByType = ($countCellsTypes > 0) ? floor($maxCells / $countCellsTypes) : $maxCells;

        switch ($spec) {
            case 'blackwhite':
                $maxRibsByElementType = $maxRibsByType;
                $maxCellsByElementType = $maxCellsByType;
                break;
            case 'countside':
                $maxRibsByElementType = $maxRibsByType;
                $maxCellsByElementType = $maxCellsByType;
                break;
            case 'completepoints':
                $maxRibsByElementType = $maxRibsByType - 1;
                $maxCellsByElementType = $maxCellsByType - 1;
                break;
            default:
                $maxRibsByElementType = $maxRibsByType - 1;
                $maxCellsByElementType = $maxCellsByType - 1;
                break;
        }

        return [
            'max_cells' => $maxCells,
            'max_ribs' => $maxRibs,
            'max_ribs_by_type' => (int)$maxRibsByType,
            'max_cells_by_type' => (int)$maxCellsByType,
            'max_ribs_by_element_type' => (int)$maxRibsByElementType,
            'max_cells_by_element_type' => (int)$maxCellsByElementType
        ];
    }

    /**
     * @brief Генерация головоломок с чёрно-белыми точкми
     *
     * @param array $max
     * @param int $m
     * @param int $n
     *
     * @return array
     */
    public function generateBlackWhiteMatrix(array $max, int $m, int $n): array
    {
        if (empty($this->allCasesMatrix)) {
            $resultArray = [];
            $step = 0;
            $this->allCasesMatrix = $this->cutMatrix($max['max_cells_by_type'], $m, $n);

            foreach ($this->allCasesMatrix as $matrixNumber => $matrix) {
                // приводим все элементы к t1 для blackwhite
                foreach ($matrix as $kRow => $rowsVal) {
                    foreach ($rowsVal as $kCol => $colsVal) {
                        $matrix[$kRow][$kCol] = LABYRINTH_WHITE_CELL_POINT;
                    }
                }

                $curHandleArray = [];
                $curMatrix = $matrix;
                $step = 0;
                while (true) {
                    if ($step > 0 && !isset($curHandleArray[$step])) {
                        break;
                    }
                    if ($step == 0) {
                        for ($i = 0; $i < $m; $i++) {
                            for ($j = 0; $j < $n; $j++) {
                                if (isset($matrix[$i][$j])) {
                                    $curMatrix = $matrix;
                                    $curMatrix[$i][$j] = LABYRINTH_BLACK_CELL_POINT;
                                    $curHandleArray[] = $curMatrix;
                                    $resultArray[] = $curMatrix;
                                }
                            }
                        }
                    } else {
                        $curMatrix = $curHandleArray[$step - 1];
                        for ($i = 0; $i < $m; $i++) {
                            for ($j = 0; $j < $n; $j++) {
                                if (isset($matrix[$i][$j])) {
                                    $t = $curMatrix;
                                    if ($t[$i][$j] != LABYRINTH_BLACK_CELL_POINT) {
                                        $t[$i][$j] = LABYRINTH_BLACK_CELL_POINT;
                                        if (!$this->isZeroMatrix($t, LABYRINTH_BLACK_CELL_POINT)
                                            && !in_array($t, $resultArray)) {
                                            $resultArray[] = $t;
                                            $curHandleArray[] = $t;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $step++;
                }
            }
            $newAr = [];
            foreach ($resultArray as $ar) {
                for ($i = 0; $i < $m; $i++) {
                    for ($j = 0; $j < $n; $j++) {
                        if (!isset($ar[$i][$j])) {
                            $ar[$i][$j] = 0;
                        }
                    }
                }
                $newAr[] = $ar;
            }
            return $newAr;
        } else {
            // случай, когда в матрице уже есть элементы, и их нужно дозаполнить
            foreach ($this->allCasesMatrix as $matrixNumber => $matrix) {
                foreach ($matrix as $kRow => $rowsVal) {
                    foreach ($rowsVal as $kCol => $colsVal) {
                        $this->allCasesMatrix[$matrixNumber][$kRow][$kCol] = LABYRINTH_WHITE_CELL_POINT;
                    }
                }
            }
            $flag = true;
            $allMatrixArray = [];
            $additionalsArray = [];
            while ($flag) {
                $flag = false;
                $tmpArray = [];
                $matrixArray = empty($allMatrixArray) ? $this->allCasesMatrix : $additionalsArray;
                foreach ($matrixArray as $matrixNumber => $matrix) {
                    foreach ($matrix as $kRow => $rowsVal) {
                        foreach ($rowsVal as $kCol => $colsVal) {
                            if ($matrix[$kRow][$kCol] == LABYRINTH_WHITE_CELL_POINT) {
                                $cloneMatrix = $matrix;
                                $cloneMatrix[$kRow][$kCol] = LABYRINTH_BLACK_CELL_POINT;
                                $tmpArray[] = $cloneMatrix;
                                $flag = true;
                            }
                        }
                    }
                }
                $additionalsArray = $tmpArray;
                $allMatrixArray = array_merge($allMatrixArray, $additionalsArray);
            }
            $this->allCasesMatrix = array_merge($this->allCasesMatrix, $additionalsArray);
            return $this->allCasesMatrix;
        }
    }

    /**
     * @brief Генерация лабиринтов с клетками, которых нужно затронуть определённое количество раз
     *
     * @param array $max
     * @param int $m
     * @param int $n
     *
     * @return array
     */
    public function generateCountSideMatrix(array $max, int $m, int $n): array
    {
        if (empty($this->allCasesMatrix)) {
            $resultArray = [];
            $this->allCasesMatrix = $this->cutMatrix($max['max_cells_by_type'], $m, $n);

            foreach ($this->allCasesMatrix as $matrixNumber => $matrix) {
                // приводим все элементы к s1 для countside
                foreach ($matrix as $kRow => $rowsVal) {
                    foreach ($rowsVal as $kCol => $colsVal) {
                        $matrix[$kRow][$kCol] = LABYRINTH_SIDE1_CELL_POINT;
                    }
                }

                $curHandleArray = [];
                $curMatrix = $matrix;
                $step = 0;
                while (true) {
                    if ($step > 0 && !isset($curHandleArray[$step])) {
                        break;
                    }
                    if ($step == 0) {
                        for ($i = 0; $i < $m; $i++) {
                            for ($j = 0; $j < $n; $j++) {
                                if (isset($matrix[$i][$j])) {
                                    $curMatrix = $matrix;
                                    if ($curMatrix[$i][$j] == LABYRINTH_SIDE1_CELL_POINT) {
                                        $curMatrix[$i][$j] = LABYRINTH_SIDE2_CELL_POINT;
                                    } else if ($curMatrix[$i][$j] == LABYRINTH_SIDE2_CELL_POINT) {
                                        $curMatrix[$i][$j] = LABYRINTH_SIDE3_CELL_POINT;
                                    }
                                    $curHandleArray[] = $curMatrix;
                                    $resultArray[] = $curMatrix;
                                }
                            }
                        }
                    } else {
                        $curMatrix = $curHandleArray[$step - 1];
                        for ($i = 0; $i < $m; $i++) {
                            for ($j = 0; $j < $n; $j++) {
                                if (isset($matrix[$i][$j])) {
                                    $t = $curMatrix;
                                    if ($t[$i][$j] != LABYRINTH_SIDE3_CELL_POINT) {
                                        if ($t[$i][$j] == LABYRINTH_SIDE1_CELL_POINT) {
                                            $t[$i][$j] = LABYRINTH_SIDE2_CELL_POINT;
                                            if (!$this->isZeroMatrix($t, LABYRINTH_SIDE1_CELL_POINT)
                                                && !in_array($t, $resultArray)) {
                                                $resultArray[] = $t;
                                                $curHandleArray[] = $t;
                                            }
                                        } else if ($t[$i][$j] == LABYRINTH_SIDE2_CELL_POINT) {
                                            $t[$i][$j] = LABYRINTH_SIDE3_CELL_POINT;
                                            if (!$this->isZeroMatrix($t, LABYRINTH_SIDE2_CELL_POINT)
                                                && !in_array($t, $resultArray)) {
                                                $resultArray[] = $t;
                                                $curHandleArray[] = $t;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $step++;
                }
            }
            $newAr = [];
            foreach ($resultArray as $ar) {
                for ($i = 0; $i < $m; $i++) {
                    for ($j = 0; $j < $n; $j++) {
                        if (!isset($ar[$i][$j])) {
                            $ar[$i][$j] = 0;
                        }
                    }
                }
                $newAr[] = $ar;
            }
            return $newAr;
        } else {
            // случай, когда в матрице уже есть элементы, и их нужно дозаполнить
            foreach ($this->allCasesMatrix as $matrixNumber => $matrix) {
                foreach ($matrix as $kRow => $rowsVal) {
                    foreach ($rowsVal as $kCol => $colsVal) {
                        $this->allCasesMatrix[$matrixNumber][$kRow][$kCol] = LABYRINTH_SIDE1_CELL_POINT;
                    }
                }
            }

            $flag = true;
            $allMatrixArray = [];
            $additionalsArray = [];
            while ($flag) {
                $flag = false;
                $tmpArray = [];
                $matrixArray = empty($allMatrixArray) ? $this->allCasesMatrix : $additionalsArray;
                foreach ($matrixArray as $matrixNumber => $matrix) {
                    foreach ($matrix as $kRow => $rowsVal) {
                        foreach ($rowsVal as $kCol => $colsVal) {
                            $cloneMatrix = $matrix;
                            if ($matrix[$kRow][$kCol] == LABYRINTH_SIDE1_CELL_POINT) {
                                $cloneMatrix[$kRow][$kCol] = LABYRINTH_SIDE2_CELL_POINT;
                            } else if ($matrix[$kRow][$kCol] == LABYRINTH_SIDE2_CELL_POINT) {
                                $cloneMatrix[$kRow][$kCol] = LABYRINTH_SIDE3_CELL_POINT;
                            }
                            if (!$this->isArrayInArray($cloneMatrix, $allMatrixArray)
                                && !$this->isArrayInArray($cloneMatrix, $tmpArray)) {
                                $tmpArray[] = $cloneMatrix;
                                $flag = true;
                            }
                        }
                    }
                }
                $additionalsArray = $tmpArray;
                $allMatrixArray = array_merge($allMatrixArray, $additionalsArray);
            }
            $this->allCasesMatrix = array_merge($this->allCasesMatrix, $allMatrixArray);

            return $this->allCasesMatrix;
        }
    }

    /**
     * @brief Генерация лабиринтов с путями, которые нужно обязательно пройти
     *
     * @param int $m
     * @param int $n
     *
     * @return array
     */
    public function generateCompletePointsMatrix(int $m, int $n): array
    {
        $result = [];

        $curMatrix = ['h' => [], 'v' => []];
        for ($i = 0; $i <= $m; $i++) {
            for ($j = 0; $j < $n; $j++) {
                $curMatrix['h'][$i][$j] = 'p';
            }
        }

        for ($i = 0; $i < $m; $i++) {
            for ($j = 0; $j <= $n; $j++) {
                $curMatrix['v'][$i][$j] = 'p';
            }
        }

        $result[] = $curMatrix;
        $matrixArray = [$curMatrix];

        $count = 0;
        while (true) {
            $flag = false;
            $matrixArrayTmp = [];

            foreach ($matrixArray as $cycleMatrix) {
                for ($i = 0; $i <= $m; $i++) {
                    for ($j = 0; $j < $n; $j++) {
                        $curMatrix = $cycleMatrix;
                        if ($curMatrix['h'][$i][$j] == LABYRINTH_RED_RIB_POINT) {
                            $curMatrix['h'][$i][$j] = 0;
                            if (!$this->isArrayInArray($curMatrix, $result)) {
                                $matrixArrayTmp[] = $curMatrix;
                                $result[] = $curMatrix;
                                $flag = true;
                            }
                        }
                    }
                }

                for ($i = 0; $i < $m; $i++) {
                    for ($j = 0; $j <= $n; $j++) {
                        $curMatrix = $cycleMatrix;
                        if ($curMatrix['v'][$i][$j] == LABYRINTH_RED_RIB_POINT) {
                            $curMatrix['v'][$i][$j] = 0;
                            if (!$this->isArrayInArray($curMatrix, $result)) {
                                $matrixArrayTmp[] = $curMatrix;
                                $result[] = $curMatrix;
                                $flag = true;
                            }
                        }
                    }
                }
            }

            $matrixArray = $matrixArrayTmp;

            if (!$flag) {
                break;
            }
            $count++;
        }
        array_pop($result);

        return $result;
    }

    /**
     * @brief Метод узнаёт, содержится один массив в другом
     *
     * @param array $needle
     * @param array $haystack
     *
     * @return bool
     */
    public function isArrayInArray(array $needle, array $haystack): bool
    {
        $curJson = json_encode($needle);
        if (false === $curJson) {
            return false;
        }
        $cur = md5($curJson);
        $curArray = [];
        foreach ($haystack as $h) {
            if (false !== json_encode($h)) {
                $curArray[] = md5(json_encode($h));
            }
        }

        return in_array($cur, $curArray);
    }

    /**
     * @brief Так как матрица особенностей одна на всех, данная матрица будет показывать,
     * сколько ячеек в ней ещё не занято и может быть использовано.
     * Но из за большого варианта перестановок, здесь будут перечислены все варианты матрицы,
     * с доступным количеством ячеек из основной
     *
     * @param int $max
     * @param int $m
     * @param int $n
     *
     * @return array
     */
    public function cutMatrix(int $max, int $m, int $n): array
    {
        $matrix = [];
        for ($i = 0; $i < $m; $i++) {
            for ($j = 0; $j < $n; $j++) {
                $matrix[$i][$j] = 1;
            }
        }
        $allMatrixes = $this->generateAccessiblenessMatrix($matrix);
        $needMatrix = [];
        foreach ($allMatrixes as $mx) {
            $count = 0;
            for ($i = 0; $i < $m; $i++) {
                for ($j = 0; $j < $n; $j++) {
                    if ($mx[$i][$j] == 1) {
                        $count++;
                    }
                }
            }
            if ($count == $max) {
                $needMatrix[] = $mx;
            }
        }
        foreach ($needMatrix as $k => $nmx) {
            for ($i = 0; $i < $m; $i++) {
                for ($j = 0; $j < $n; $j++) {
                    if ($nmx[$i][$j] == 0) {
                        unset($needMatrix[$k][$i][$j]);
                    }
                }
            }
        }

        return $needMatrix;
    }

    /**
     * @brief матрицы особенностей для данной матрицы
     *
     * @param array $specials
     * @param array $dim
     *
     * @return array
     */
    public function generateSpecialsMatrix(array $specials, array $dim)
    {
        $result = ['cells' => [], 'ribs' => [], 'type' => $specials];

        $m = count($dim['V']);
        $n = count($dim['H'][0]);

        $countCellsTypes = 0;
        $countRibsTypes = 0;
        $typesCells = ['blackwhite', 'countside'];
        $typesRibs = ['completepoints'];

        // а тут разбиваем и группируем типы для текущей генерации
        $currentCells = [];
        $currentRibs = [];
        foreach ($specials as $s) {
            if (in_array($s, $typesCells)) {
                $countCellsTypes++;
                $currentCells[] = $s;
            }
            if (in_array($s, $typesRibs)) {
                $countRibsTypes++;
                $currentRibs[] = $s;
            }
        }
        $allSpecials = $this->generateSpesialsArray(array_merge($currentCells, $currentRibs));

        foreach ($allSpecials as $k => $spec) {
            foreach ($spec as $s) {
                $max = $this->calculateMaxCountElementsByType($s, $countCellsTypes, $countRibsTypes, $dim);
                switch ($s) {
                    case 'blackwhite' :
                        $blackwhite = $this->generateBlackWhiteMatrix($max, $m, $n);
                        $result['cells'] = array_merge($result['cells'], $blackwhite);
                        break;
                    case 'completepoints' :
                        $completePoints = $this->generateCompletePointsMatrix($m, $n);
                        $result['ribs'] = array_merge($result['ribs'], $completePoints);
                        break;
                    case 'countside' :
                        $countside = $this->generateCountSideMatrix($max, $m, $n);
                        $result['cells'] = array_merge($result['cells'], $countside);
                        break;
                }
            }
        }

        return $result;
    }

    /**
     * @param array $specials
     *
     * @return array
     */
    public function generateAllWays(array $specials): array
    {
        $horizontal = $specials['dim']['H'];
        $vertical = $specials['dim']['V'];
        $start = $specials['starts'];
        $end = $specials['ends'];

        $allRibs = [];

        for ($i = 0; $i < count($horizontal); $i++) {
            for ($j = 0; $j < count($horizontal[0]); $j++) {
                if ($horizontal[$i][$j] == 1) {
                    $allRibs[] = 'a_' . ($i + 1) . "_" . ($j + 1);
                }
            }
        }
        for ($i = 0; $i < count($vertical); $i++) {
            for ($j = 0; $j < count($vertical[0]); $j++) {
                if ($vertical[$i][$j] == 1) {
                    $allRibs[] = 'b_' . ($i + 1) . "_" . ($j + 1);
                }
            }
        }
        $startRibs = [];
        foreach ($start as $s) {
            $startRibs[] = "a_" . ($s->m) . "_" . ($s->n - 1);
            $startRibs[] = "a_" . ($s->m) . "_" . ($s->n);
            $startRibs[] = "b_" . ($s->m - 1) . "_" . ($s->n);
            $startRibs[] = "b_" . ($s->m) . "_" . ($s->n);
        }

        // отсеиваем несуществующие рёбра
        $tmpS = [];
        foreach ($startRibs as $sr) {
            if (in_array($sr, $allRibs)) {
                $tmpS[] = $sr;
            }
        }

        $endRibs = [];
        foreach ($end as $e) {
            $endRibs[] = "a_" . ($e->m) . "_" . ($e->n - 1);
            $endRibs[] = "a_" . ($e->m) . "_" . ($e->n);
            $endRibs[] = "b_" . ($e->m - 1) . "_" . ($e->n);
            $endRibs[] = "b_" . ($e->m) . "_" . ($e->n);
        }
        // отсеиваем несуществующие рёбра
        $tmpE = [];
        foreach ($endRibs as $er) {
            if (in_array($er, $allRibs)) {
                $tmpE[] = $er;
            }
        }

        //Начинаем строить цепочки
        $result = [];

        foreach ($tmpS as $t) {
            $all = $allRibs;
            foreach ($all as $k => $v) {
                if ($t == $v) {
                    unset($all[$k]);
                }
            }

            $curRib = $t;
            $tmpResult = [['all' => $all, 'ribs' => [$curRib]]];
            $curCount = count($tmpResult);
            while (true) {
                // находим всевозможные соседние рёбра
                $curTmpResult = [];

                foreach ($tmpResult as $kTr => $tr) {
                    $allExistingRibs = [];
                    if (empty($tr['all'])) {
                        continue;
                    }
                    $tmpAll = $tr['all'];
                    $curRib = $tr['ribs'][count($tr['ribs']) - 1];

                    $curRibArr = explode("_", $curRib);
                    if ($curRibArr[0] == 'a') {
                        $allExistingRibs = [
                            'a_' . ($curRibArr[1]) . "_" . ((int)$curRibArr[2] - 1),
                            'a_' . ($curRibArr[1]) . "_" . ((int)$curRibArr[2] + 1),
                            'b_' . ((int)$curRibArr[1] - 1) . "_" . ($curRibArr[2]),
                            'b_' . ($curRibArr[1]) . "_" . ($curRibArr[2]),
                            'b_' . ((int)$curRibArr[1] - 1) . "_" . ((int)$curRibArr[2] + 1),
                            'b_' . ($curRibArr[1]) . "_" . ((int)$curRibArr[2] + 1)
                        ];
                    } else if ($curRibArr[0] == 'b') {
                        $allExistingRibs = [
                            'b_' . ((int)$curRibArr[1] - 1) . "_" . ($curRibArr[2]),
                            'b_' . ((int)$curRibArr[1] + 1) . "_" . ($curRibArr[2]),
                            'a_' . ($curRibArr[1]) . "_" . ((int)$curRibArr[2] - 1),
                            'a_' . ($curRibArr[1]) . "_" . ($curRibArr[2]),
                            'a_' . ((int)$curRibArr[1] + 1) . "_" . ((int)$curRibArr[2] - 1),
                            'a_' . ((int)$curRibArr[1] + 1) . "_" . ($curRibArr[2])
                        ];
                    }

                    //выделяем из них те, которые существуют
                    $accessed = [];
                    foreach ($allExistingRibs as $r) {
                        if (in_array($r, $tmpAll)) {
                            $accessed[] = $r;
                        }
                    }

                    foreach ($accessed as $ac) {
                        $ttt = $tr['ribs'];
                        $ttt[] = $ac;
                        foreach ($tmpAll as $kk => $vv) {
                            if ($ac == $vv) {
                                unset($tmpAll[$kk]);
                            }
                        }

                        $curTmpResult[] = ['ribs' => $ttt, 'all' => $tmpAll];
                        if (in_array($ac, $tmpE)) {
                            $result[] = $ttt;
                        }
                    }
                }
                $tmpResult = $curTmpResult;

                // условие для выхода
                if (count($tmpResult) == $curCount) {
                    break;
                } else {
                    $curCount = count($tmpResult);
                }
            }
        }
        $tResult = $result;
        $result = [];
        foreach ($tResult as $trt) {
            if ($this->isCorrectWay($trt, $start, $end)) {
                $result[] = $trt;
            }
        }
        $tResult = $result;
        $result = [];
        foreach ($tResult as $r) {
            $result[] = implode(":", $r);
        }

        return $result;
    }

    /**
     * @param array $way
     * @param array $starts
     * @param array $ends
     *
     * @return bool
     */
    public function isCorrectWay(array $way, array $starts, array $ends): bool
    {
        $starts = $this->objectToArray($starts);
        $ends = $this->objectToArray($ends);
        $usedPoint = [];
        $flag = true;
        $wayCount = count($way);
        $wArr = explode("_", $way[0]);
        // проверяем - верное ли начало
        if ($wArr[0] == 'a') {
            foreach ($starts as $s) {
                if ($wArr[1] != $s['m'] || $wArr[2] != $s['n']) {
                    return false;
                }
            }
        } else {
            foreach ($starts as $s) {
                if (((int)$wArr[1] + 1) != $s['m'] || $wArr[2] != $s['n']) {
                    return false;
                }
            }
        }
        $wArr = explode("_", $way[$wayCount - 1]);
        // проверяем - верный ли конец
        if ($wArr[0] == 'a') {
            foreach ($ends as $e) {
                if ($wArr[1] != $e['m'] || ((int)$wArr[2] + 1) != $e['n']) {
                    return false;
                }
            }
        } else {
            foreach ($ends as $e) {
                if ($wArr[1] != $e['m'] || $wArr[2] != $e['n']) {
                    return false;
                }
            }
        }

        foreach ($way as $kkk => $w) {
            $wArr = explode("_", $w);
            if ($wArr[0] == 'a') {
                if (isset($usedPoint[$wArr[1]][$wArr[2]])) {
                    if ($usedPoint[$wArr[1]][$wArr[2]] <= 1) {
                        $usedPoint[$wArr[1]][$wArr[2]]++;
                    } else {
                        $flag = false;
                    }
                } else {
                    $usedPoint[$wArr[1]][$wArr[2]] = 0;
                }
                if (isset($usedPoint[$wArr[1]][(int)$wArr[2] + 1])) {
                    if ($usedPoint[$wArr[1]][(int)$wArr[2] + 1] <= 1) {
                        $usedPoint[$wArr[1]][(int)$wArr[2] + 1]++;
                    } else {
                        $flag = false;
                    }
                } else {
                    $usedPoint[$wArr[1]][(int)$wArr[2] + 1] = 0;
                }
            } else {
                // b
                if (isset($usedPoint[$wArr[1]][$wArr[2]])) {
                    if ($usedPoint[$wArr[1]][$wArr[2]] <= 1) {
                        $usedPoint[$wArr[1]][$wArr[2]]++;
                    } else {
                        $flag = false;
                    }
                } else {
                    $usedPoint[$wArr[1]][$wArr[2]] = 0;
                }

                if (isset($usedPoint[(int)$wArr[1] + 1][$wArr[2]])) {
                    if ($usedPoint[(int)$wArr[1] + 1][$wArr[2]] <= 1) {
                        $usedPoint[(int)$wArr[1] + 1][$wArr[2]]++;
                    } else {
                        $flag = false;
                    }
                } else {
                    $usedPoint[(int)$wArr[1] + 1][$wArr[2]] = 0;
                }
            }
        }
        foreach ($usedPoint as $up) {
            foreach ($up as $i) {
                if ($i > 1) {
                    $flag = false;
                }
            }
        }

        if ($usedPoint[$starts[0]['m']][$starts[0]['n']] > 0 || $usedPoint[$ends[0]['m']][$ends[0]['n']] > 0) {
            $flag = false;
        }

        return $flag;
    }

    /**
     * @brief Генерация головоломки по переданным параметрам
     *
     * @param int $i
     * @param int $j
     * @param array $spec
     * @param string $reg
     *
     * @return int
     */
    public function generateByParams(int $i, int $j, array $spec, string $reg)
    {
        $dim = [];
        $points = [];
        $all = 0;
        $lk = new LabyrinthController();
        $lm = new LabyrinthModule();

        $square = $i * $j;
        // 2. Создаём матрицы размерности лабиринта (полные без разрывов)
        $dim[0]['H'] = $this->generateByDimensions($i, $j, 'h');
        $dim[0]['V'] = $this->generateByDimensions($i, $j, 'v');

        // 3. Транспонируем матрицы для дополнительного вида разметок
        $dim[1]['H'] = $this->generateByDimensions($j, $i, 'h');
        $dim[1]['V'] = $this->generateByDimensions($j, $i, 'v');

        // 5. Определяем количество входов и выходов
        $maxCountStarts = floor($square / 10) + 1;
        $maxCountEnds = floor($square / 10) + 1;

        /**
         * 6. Генерируем входы и выходы
         * Ввиду огромнейшего числа лабиринтов сделано упращение
         * на левый нижний вход и правый верхний выход
         */

        $pointsStarts = [
            [['m' => ($i + 1), 'n' => 1]],
            [['m' => ($j + 1), 'n' => 1]]
        ];
        $pointsEnds = [
            [['m' => 1, 'n' => ($j + 1)]],
            [['m' => 1, 'n' => ($i + 1)]]
        ];

        // 7. Проходим по обычной и транспонированной матрице
        for ($a = 0; $a <= 1; $a++) {
            // 8. Генерируем особенности матрицы
            $sMatrixT = $this->generateSpecialsMatrix($spec, $dim[$a]);

            // 9. Генерируем разрывности матрицы
            $matrix['H'] = $this->generateAccessiblenessMatrix($dim[$a]['H']);
            $matrix['V'] = $this->generateAccessiblenessMatrix($dim[$a]['V']);

            // 10. Генерация
            for ($k = 0; $k < count($matrix['H']); $k++) {
                for ($kk = 0; $kk < count($matrix['V']); $kk++) {
                    $accessMatrix = [];
                    $accessMatrix['H'] = $matrix['H'][$k];
                    $accessMatrix['V'] = $matrix['V'][$kk];

                    $spUnit = [];
                    $baseSpecials = [
                        'cellMatrix' => [],
                        'pathMatrix' => [],
                        'start' => $pointsStarts[$a],
                        'end' => $pointsEnds[$a]
                    ];

                    if (empty($sMatrixT['cells'])) {
                        foreach ($sMatrixT['ribs'] as $rib) {
                            $baseSpecials['pathMatrix'] = $rib;
                            $spUnit[] = $baseSpecials;
                        }
                    } else if (empty($sMatrixT['ribs'])) {
                        foreach ($sMatrixT['cells'] as $cell) {
                            $baseSpecials['cellMatrix'] = $cell;
                            $spUnit[] = $baseSpecials;
                        }
                    } else {
                        foreach ($sMatrixT['cells'] as $cell) {
                            foreach ($sMatrixT['ribs'] as $rib) {
                                $baseSpecials['pathMatrix'] = $rib;
                                $baseSpecials['cellMatrix'] = $cell;
                                $spUnit[] = $baseSpecials;
                            }
                        }
                    }

                    foreach ($spUnit as $sp) {
                        $params = [
                            'm' => $a === 0 ? $i : $j,
                            'n' => $a === 0 ? $j : $i,
                            'specials' => $sp,
                            'solution' => '',
                            'accessible_way' => [
                                'horizont' => $accessMatrix['H'],
                                'vertical' => $accessMatrix['V']
                            ],
                            'types' => $spec,
                            'region' => $reg
                        ];

                        $spp = [
                            'dim' => [
                                'H' => $accessMatrix['H'],
                                'V' => $accessMatrix['V']
                            ],
                            'starts' => [(object)$pointsStarts[$a][0]],
                            'ends' => [(object)$pointsEnds[$a][0]]
                        ];

                        $allSolves = $this->generateAllWays($spp);
                        if (count($allSolves) > 0) {
                            foreach ($allSolves as $solve) {
                                $resultSolve = $a == 0 ? $lm->isSolved($i, $j, $solve, $sMatrixT['type'], $sp) :
                                    $lm->isSolved($j, $i, $solve, $sMatrixT['type'], $sp);
                                if ($resultSolve) {
                                    $params['solution'] = $solve;
                                    $lk->save($params);
                                    $all++;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $all;
    }
}
