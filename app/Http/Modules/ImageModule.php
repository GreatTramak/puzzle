<?php

namespace App\Http\Modules;

use App\Http\Traits\Helper;

/**
 * Class ImageModule
 *
 * @package App\Http\Modules
 */
class ImageModule
{
    use Helper;

    /**
     * @param int $regionId
     * @param array $maps
     *
     * @return array|false|null
     */
    public function getRegionFromDataById(int $regionId, array $maps)
    {
        $colors = [];
        foreach ($maps as $m) {
            foreach ($m['regions'] as $r) {
                if ($r['id'] == $regionId) {
                    $colors = $r['colors'];
                    break 2;
                }
            }
        }
        if (empty($colors)) {
            return null;
        }
        $color = $colors[rand(0, count($colors) - 1)];

        return $this->hexRgbToDecArray($color['color']);
    }

    /**
     * Convert color string of hexadecimal to array of decimal
     *
     * Convert a string "#rrggbb" or "#rgb" (in hexadecimal form)
     * to an array('r' => r, 'g' => g, 'b' => b) with decimal values.
     *
     * @param string $hexRgb Hexadecimal color string
     *
     * @return array|false   Array of decimal values or false,
     *                       if hexadecimal string invalid
     */
    public function hexRgbToDecArray($hexRgb)
    {
        $hexRgb = (string)$hexRgb;
        if (!$hexRgb) {
            return false;
        }
        $result = [];
        if (substr($hexRgb, 0, 1) == '#') {
            $hexRgb = substr($hexRgb, 1);
        }
        $hexRgbLen = strlen($hexRgb);
        if (6 == $hexRgbLen) {
            $result['r'] = hexdec(substr($hexRgb, 0, 2));
            $result['g'] = hexdec(substr($hexRgb, 2, 2));
            $result['b'] = hexdec(substr($hexRgb, 4, 2));
        } elseif (3 == $hexRgbLen) {
            $result['r'] = hexdec(substr($hexRgb, 0, 1));
            $result['g'] = hexdec(substr($hexRgb, 1, 1));
            $result['b'] = hexdec(substr($hexRgb, 2, 1));
        } else {
            return false;
        }

        return $result;
    }

    /**
     * @param mixed $img
     * @param int $category
     *
     * @return resource
     */
    public function addEffectByRegionCategory($img, int $category)
    {
        if ($category == 1) {
            $indexEffect = rand(1, 2);
            switch ($indexEffect) {
                case 1:
                    $filtered = imagefilter($img, IMG_FILTER_MEAN_REMOVAL);
                    break;
                case 2:
                    $filtered = imagefilter($img, IMG_FILTER_SMOOTH, 1);
                    break;
            }
        }

        //$filtered = imagefilter($img, IMG_FILTER_PIXELATE, 10);
        //$filtered = imagefilter($img, IMG_FILTER_NEGATE);
        //$filtered = imagefilter($img, IMG_FILTER_GRAYSCALE);
        //$filtered = imagefilter($img, IMG_FILTER_BRIGHTNESS, -10);
        //$filtered = imagefilter($img, IMG_FILTER_CONTRAST, -20);
        //$filtered = imagefilter($img, IMG_FILTER_COLORIZE, 10, 105, 255, 50);
        //$filtered = imagefilter($img, IMG_FILTER_EDGEDETECT);
        //$filtered = imagefilter($img, IMG_FILTER_EMBOSS);
        //$filtered = imagefilter($img, IMG_FILTER_GAUSSIAN_BLUR);
        //$filtered = imagefilter($img, IMG_FILTER_SELECTIVE_BLUR);
        //$filtered = imagefilter($img, IMG_FILTER_MEAN_REMOVAL);
        //$filtered = imagefilter($img, IMG_FILTER_SMOOTH, 1);

        return $img;
    }
}
