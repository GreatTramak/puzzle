<?php

namespace App\Http\Modules;

use App\Http\Traits\Helper;
use Illuminate\Support\Facades\DB;

/**
 * Class UserModule
 *
 * @package App\Http\Modules
 */
class UserModule
{
    use Helper;

    /**
     * @param mixed $user
     *
     * @param array $params
     */
    public function completePuzzle($user, array $params): void
    {
        $params['user_id'] = $user->id;

        DB::table('users_puzzles')->insert($params);

        // считаем уровень
        $levels = DB::select('SELECT * FROM `users_experience` WHERE true');
        $levels = $this->objectToArray($levels);

        $lvl = $user->level;
        $exp = $user->experience + 1;

        foreach ($levels as $l) {
            if ($l['experience'] == $exp) {
                $lvl = $l['level'];
                break;
            }
        }

        $user->level = $lvl;
        $user->experience = $exp;
        $user->save();
    }
}
