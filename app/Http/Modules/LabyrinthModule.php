<?php

namespace App\Http\Modules;

/**
 * Class LabyrinthModule
 *
 * @package App\Http\Modules
 */
class LabyrinthModule
{
    /**
     * @var int $m
     */
    public $m = 1; // строки

    /**
     * @var int $n
     */
    public $n = 2; // столбцы

    /**
     * @var string $specials
     */
    public $specials; // матрица особенностей

    /**
     * @var string $accessible_way
     */
    public $accessible_way; // матрица доступного пути

    /**
     * @var string $solution
     */
    public $solution; // предлагаемое решение

    /**
     * @var bool $status
     */
    public $status; // активен ли

    /**
     * LabyrinthModule constructor.
     *
     * @param array $params
     */
    public function __construct($params = [])
    {
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @return int
     */
    public function calculateCellLength()
    {
        switch ($this->n) {
            case $this->n < 3:
                return 200;
            case $this->n < 6;
                return 100;
            case $this->n < 11:
                return 70;
            default:
                return 100;
        }
    }

    /**
     * @return int
     */
    public function calculateLabyrinthOffsetHorizont()
    {
        switch ($this->n) {
            case $this->n == 1:
                return 270;
            case $this->n < 3:
                return 180;
            case $this->n < 6;
                return 230;
            case $this->n < 11:
                return 70;
            default:
                return 100;
        }
    }

    /**
     * @return int
     */
    public function calculateLabyrinthOffsetVertical()
    {
        switch ($this->m) {
            case $this->m < 3:
                return 50;
            case $this->m < 6;
                return 30;
            case $this->m < 11:
                return 10;
            default:
                return 20;
        }
    }

    /**
     * @brief Определяет направление обхода
     *
     * @param int $step
     * @param array $ribs
     *
     * @return string|null
     */
    public function chooseDestination(int $step, array $ribs)
    {
        $info = explode("_", $ribs[$step]);
        $previousThisType = [];
        $prevOtherType = [];
        $nextThisType = [];
        $nextOtherType = [];

        // горизонтально ли ребро
        if ($info[0] == 'a') {
            for ($i = $step - 1; $i >= 0; $i--) {
                $ribInfo = explode("_", $ribs[$i]);
                if ($ribInfo[0] == 'a') {
                    $previousThisType = $ribInfo;
                    break;
                }
            }
            // смотрим, есть ли ещё элементы этого же типа ранее
            if (count($previousThisType) === 0) {
                // если он первый, смотрим по следующему
                for ($i = $step + 1; $i < count($ribs); $i++) {
                    $ribInfo = explode("_", $ribs[$i]);
                    if ($ribInfo[0] == 'a') {
                        $nextThisType = $ribInfo;
                        break;
                    }
                }
                if (count($nextThisType) === 0) {
                    if ($i >= (count($ribs) - 1)) {
                        $i = $step;
                    }
                    // значит в пути всего один горизонтальный элемент
                    // смотрим по вертикали
                    // здесь находим следующее вертикальное ребро
                    for ($j = $i; $j < count($ribs); $j++) {
                        $ribInfo = explode("_", $ribs[$j]);
                        if ($ribInfo[0] == 'b') {
                            $nextOtherType = $ribInfo;
                            break;
                        }
                    }
                    if (count($nextOtherType) == 0) {
                        // это ситуация когда всего один горизонтальный элемент, и он последний
                        // значит находим предыдущий вертикальный
                        for ($j = $i; $j >= 0; $j--) {
                            $ribInfo = explode("_", $ribs[$j]);

                            if ($ribInfo[0] == 'b') {
                                $prevOtherType = $ribInfo;
                                break;
                            }
                        }
                        if (count($prevOtherType) == 0) {
                            return null;
                        } else {
                            if ($prevOtherType[2] == $info[2]) {
                                return 'right';
                            } else {
                                return 'left';
                            }
                        }

                    } else {
                        if ($nextOtherType[2] == $info[2]) {
                            return 'left';
                        } else {
                            return 'right';
                        }
                    }
                } else {
                    if ($nextThisType[2] > $info[2]) {
                        return 'right';
                    } else if ($nextThisType[2] < $info[2]) {
                        return 'left';
                    } else if ($nextThisType[2] == $info[2]) {
                        $i = $step;
                        // здесь находим следующее вертикальное ребро
                        for ($j = $i; $j < count($ribs); $j++) {
                            $ribInfo = explode("_", $ribs[$j]);
                            if ($ribInfo[0] == 'b') {
                                $nextOtherType = $ribInfo;
                                break;
                            }
                        }
                        if (count($nextOtherType) == 0) {
                            // попытаться представить - возможно ли так?
                            // нет, так не бывает, что же вернуть?
                            // верну false - как знак того, что есть ошибка в маршруте
                            // скорее всего это решегие где всего одна черта, верну right как значение по умолчанию
                            return 'right';
                        } else {
                            if ($nextOtherType[2] == $info[2]) {
                                return 'left';
                            } else {
                                return 'right';
                            }
                        }
                    }
                }
            } else {
                if ($previousThisType[2] < $info[2]) {
                    return 'right';
                } else if ($previousThisType[2] > $info[2]) {
                    return 'left';
                } else if ($previousThisType[2] == $info[2]) {
                    // здесь находим предыдущее вертикальное ребро
                    $i = $step;
                    for ($j = $i; $j >= 0; $j--) {
                        $ribInfo = explode("_", $ribs[$j]);
                        if ($ribInfo[0] == 'b') {
                            $prevOtherType = $ribInfo;
                            break;
                        }
                    }
                    if (count($prevOtherType) == 0) {
                        // попытаться представить - возможно ли так?
                        // нет, так не бывает, что же вернуть?
                        // верну false - как знак того, что есть ошибка в маршруте
                        // скорее всего это решегие где всего одна черта, верну right как значение по умолчанию
                        return 'right';
                    } else {
                        if ($prevOtherType[2] == $info[2]) {
                            return 'right';
                        } else {
                            return 'left';
                        }
                    }
                }
            }
        } // иначе вертикальное
        else {
            for ($i = $step - 1; $i >= 0; $i--) {
                $ribInfo = explode("_", $ribs[$i]);
                if ($ribInfo[0] == 'b') {
                    $previousThisType = $ribInfo;
                    break;
                }
            }
            // смотрим, есть ли ещё элементы этого же типа ранее
            if (count($previousThisType) === 0) {
                // если он первый, смотрим по следующему
                for ($i = $step + 1; $i < count($ribs); $i++) {
                    $ribInfo = explode("_", $ribs[$i]);
                    if ($ribInfo[0] == 'b') {
                        $nextThisType = $ribInfo;
                        break;
                    }
                }
                if (count($nextThisType) === 0) {
                    if ($i >= (count($ribs) - 1)) {
                        $i = $step;
                    }
                    // значит в пути всего один вертикальный элемент
                    // смотрим по горизонтали
                    // здесь находим следующее горизонтальное ребро
                    //echo $i;
                    for ($j = $i; $j < count($ribs); $j++) {
                        $ribInfo = explode("_", $ribs[$j]);
                        if ($ribInfo[0] == 'a') {
                            $nextOtherType = $ribInfo;
                            break;
                        }
                    }
                    if (count($nextOtherType) == 0) {
                        // попытаться представить - возможно ли так?
                        // нет, так не бывает, что же вернуть?
                        // верну false - как знак того, что есть ошибка в маршруте
                        // скорее всего это решегие где всего одна черта, верну up как значение по умолчанию
                        return 'up';
                    } else {
                        if ($nextOtherType[1] == $info[1]) {
                            return 'up';
                        } else {
                            return 'down';
                        }
                    }
                } else {
                    if ($nextThisType[1] < $info[1]) {
                        return 'up';
                    } else if ($nextThisType[1] > $info[1]) {
                        return 'down';
                    } else if ($nextThisType[1] == $info[1]) {
                        if ($i >= count($ribs) - 1) {
                            $i = $step;
                        }
                        // здесь находим следующее горизонтальное ребро
                        for ($j = $i; $j < count($ribs); $j++) {
                            $ribInfo = explode("_", $ribs[$j]);
                            if ($ribInfo[0] == 'a') {
                                $nextOtherType = $ribInfo;
                                break;
                            }
                        }
                        if (count($nextOtherType) == 0) {
                            // попытаться представить - возможно ли так?
                            // нет, так не бывает, что же вернуть?
                            // верну false - как знак того, что есть ошибка в маршруте
                            // скорее всего это решегие где всего одна черта, верну up как значение по умолчанию
                            return 'up';
                        } else {
                            if ($nextOtherType[1] == $info[1]) {
                                return 'up';
                            } else {
                                return 'down';
                            }
                        }
                    }
                }
            } else {
                if ($previousThisType[1] > $info[1]) {
                    return 'up';
                } else if ($previousThisType[1] < $info[1]) {
                    return 'down';
                } else if ($previousThisType[1] == $info[1]) {
                    if ($i <= 0) {
                        $i = $step;
                    }
                    // здесь находим предыдущее горизонтальное ребро
                    for ($j = $step; $j >= 0; $j--) {
                        $ribInfo = explode("_", $ribs[$j]);
                        if ($ribInfo[0] == 'a') {
                            $prevOtherType = $ribInfo;
                            break;
                        }
                    }
                    if (count($prevOtherType) == 0) {
                        // попытаться представить - возможно ли так?
                        // нет, так не бывает, что же вернуть?
                        // верну false - как знак того, что есть ошибка в маршруте
                        // скорее всего это решегие где всего одна черта, верну up как значение по умолчанию
                        return 'up';
                    } else {
                        if ($prevOtherType[1] == $info[1]) {
                            return 'down';
                        } else {
                            return 'up';
                        }
                    }
                }
            }
        }

        return null;
    }

    /**
     * @brief Функция для разделения выделенных путём областей, чтобы сравнивать области по различным критериям
     *
     * @param array $labyrinth
     *
     * @return array
     */
    public function cutFields(array $labyrinth): array
    {
        foreach ($labyrinth as $kl => $vl) {
            foreach ($vl as $k => $v) {
                if ($v == 'L') {
                    $labyrinth[$kl][$k] = 1;
                }
                if ($v == 'R') {
                    $labyrinth[$kl][$k] = 0;
                }
            }
        }

        $fields = [];
        $current = null;
        $kColumn = 0;
        while (!empty($labyrinth)) {
            foreach ($labyrinth as $kLine => $line) {
                foreach ($line as $kColumn => $Column) {
                    if ($labyrinth[$kLine][$kColumn] != 0) {
                        $current = ['line' => $kLine, 'column' => $kColumn];
                        break 2;
                    }
                }
            }

            $readyLabyrinth = false;
            $available = [];
            $count = 0;

            if (!$current) {
                return [];
            }

            while (!$readyLabyrinth) {
                //уже рассмотрена, поэтому 2
                $available[$kLine][$kColumn] = 2;
                unset($labyrinth[$kLine][$kColumn]);
                while (true) {
                    $enterFlag = true;

                    if (isset($labyrinth[$current['line']][$current['column'] + 1]) && $labyrinth[$current['line']][$current['column'] + 1] > 0) {
                        $available[$current['line']][$current['column'] + 1] = 1;
                        $enterFlag = false;
                    }
                    if (isset($labyrinth[$current['line']][$current['column'] - 1]) && $labyrinth[$current['line']][$current['column'] - 1] > 0) {
                        $available[$current['line']][$current['column'] - 1] = 1;
                        $enterFlag = false;
                    }
                    if (isset($labyrinth[$current['line'] + 1][$current['column']]) && $labyrinth[$current['line'] + 1][$current['column']] > 0) {
                        $available[$current['line'] + 1][$current['column']] = 1;
                        $enterFlag = false;
                    }
                    if (isset($labyrinth[$current['line'] - 1][$current['column']]) && $labyrinth[$current['line'] - 1][$current['column']] > 0) {
                        $available[$current['line'] - 1][$current['column']] = 1;
                        $enterFlag = false;
                    }
                    foreach ($available as $kAvailableLine => $l) {
                        foreach ($l as $kAvailableColumn => $c) {
                            if ($c == 1) {
                                $current = ['line' => $kAvailableLine, 'column' => $kAvailableColumn];
                                $available[$kAvailableLine][$kAvailableColumn] = 2;
                                unset($labyrinth[$kAvailableLine][$kAvailableColumn]);
                                $enterFlag = false;
                                break 2;
                            }
                        }
                    }

                    if ($enterFlag) {
                        $fields[] = $available;
                        $readyLabyrinth = true;
                        break;
                    }
                }

            }
            //проверяем - если остались только нули в лабиринте - формируем условие для выхода
            $flagEmpty = true;
            foreach ($labyrinth as $kLine => $line) {
                foreach ($line as $kColumn => $Column) {
                    if ($labyrinth[$kLine][$kColumn] != 0) {
                        $flagEmpty = false;
                        break 2;
                    }
                }
            }
            if ($flagEmpty) {
                $labyrinth = [];
            }
        }

        return $fields;
    }

    /**
     * @param array $end
     * @param array $max
     *
     * @return array
     */
    public function calculateEndLine(array $end, array $max): array
    {
        // смотримвлево или вправо будет уходить выход
        if ($end['m'] == 1 || $end['m'] == $max['m']) {
            if ($end['m'] == 1) {
                $dm = -1;
            } else {
                $dm = 1;
            }
        } else {
            $dm = 0;
        }

        if ($end['n'] == 1 || $end['n'] == $max['n']) {
            if ($end['n'] == 1) {
                $dn = -1;
            } else {
                $dn = 1;
            }
        } else {
            $dn = 0;
        }

        return ['dm' => $dm, 'dn' => $dn];
    }

    /**
     * @param array $field1
     * @param array $field2
     * @param int $m
     * @param int $n
     *
     * @return array
     */
    public function mergeFields(array $field1, array $field2, int $m, int $n): array
    {
        $field = [];
        $count = 0;
        $column = null;
        foreach ($field1 as $k_line => $line) {
            foreach ($line as $k_column => $column) {
                $count++;
                if (!isset($field[$k_line][$k_column])) {
                    $field[$k_line][$k_column] = $column;
                }
            }
        }
        foreach ($field2 as $k_line => $line) {
            foreach ($line as $k_column => $column) {
                $count++;
                if (!isset($field[$k_line][$k_column])) {
                    $field[$k_line][$k_column] = $column;
                }
            }
        }
        $originSize = ($m - 1) * ($n - 1);
        if ($count < $originSize) {
            for ($i = 0; $i < $m - 1; $i++) {
                for ($j = 0; $j < $n - 1; $j++) {
                    if (!isset($field[$i][$j])) {
                        $field[$i][$j] = $column;
                    }
                }
            }
        }

        return $field;
    }

    /**
     * @brief Функция для дозаполнения
     *
     * @param array $labyrinth
     * @param array $secondLabyrinth
     * @param int $m
     * @param int $n
     *
     * @return array
     */
    public function fillFields(array $labyrinth, array $secondLabyrinth, int $m, int $n): array
    {
        if (empty($labyrinth)) {
            return [];
        }
        foreach ($labyrinth as $k_line => $line) {
            foreach ($line as $k_column => $column) {
                if ($column == 'L') {
                    for ($i = $k_column - 1; $i >= 0; $i--) {
                        if (!isset($labyrinth[$k_line][$i]) && !isset($secondLabyrinth[$k_line][$i])) {
                            $labyrinth[$k_line][$i] = 'L';
                        }
                    }
                    for ($i = $k_column + 1; $i < $n - 1; $i++) {
                        if (!isset($labyrinth[$k_line][$i]) && !isset($secondLabyrinth[$k_line][$i])) {
                            $labyrinth[$k_line][$i] = 'L';
                        }
                    }
                }
                if ($column == 'R') {
                    for ($i = $k_column + 1; $i < $n - 1; $i++) {
                        if (!isset($labyrinth[$k_line][$i]) && !isset($secondLabyrinth[$k_line][$i])) {
                            $labyrinth[$k_line][$i] = 'R';
                        }
                    }
                    for ($i = $k_column - 1; $i >= 0; $i--) {
                        if (!isset($labyrinth[$k_line][$i]) && !isset($secondLabyrinth[$k_line][$i])) {
                            $labyrinth[$k_line][$i] = 'R';
                        }
                    }
                }
            }
        }

        return $labyrinth;
    }

    /**
     * @param array $labyrinth
     *
     * @return array
     */
    public function revertLabyrinth(array $labyrinth): array
    {
        foreach ($labyrinth as $k_line => $line) {
            foreach ($line as $k_column => $column) {
                if ($labyrinth[$k_line][$k_column] == 'L') {
                    $labyrinth[$k_line][$k_column] = 'R';
                } else if ($labyrinth[$k_line][$k_column] == 'R') {
                    $labyrinth[$k_line][$k_column] = 'L';
                }
            }
        }

        return $labyrinth;
    }

    /**
     * @brief $pathString - строка пути, читается следующим образом:
     * a_5_1
     * первая буква, если а - горизонтальное ребро, b - вертикальное ребро
     * второе число - номер строки в матрице пути ребра
     * третье число - номер столбца в матрице пути ребра
     *
     * @param int $m
     * @param int $n
     * @param string $pathString
     * @param array $types
     * @param array $specials
     *
     * @return bool
     */
    public function isSolved(int $m, int $n, string $pathString, array $types, array $specials): bool
    {
        $m++;
        $n++;
        //выделение областей
        $ribs = explode(":", $pathString);
        $innerPlane = []; // 1
        $outerPlane = []; // 2

        foreach ($ribs as $key_rib => $rib) {
            $currentRib = explode("_", $rib);
            $destination = $this->chooseDestination($key_rib, $ribs);
            //если оно горизонтальное
            if ($currentRib[0] == 'a') {
                //смотрим, граничное ли оно
                if ($currentRib[1] == $m || $currentRib[1] == 1) {
                    if ($currentRib[1] == 1) {
                        if ($destination == 'right') {
                            if (!isset($outerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1])) {
                                $outerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1] = 'R';
                            }
                        } else {
                            if (!isset($innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1])) {
                                $innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1] = 'L';
                            }
                        }
                    } else {
                        if ($destination == 'right') {
                            if (!isset($innerPlane[(int)$currentRib[1] - 2][(int)$currentRib[2] - 1])) {
                                $innerPlane[(int)$currentRib[1] - 2][(int)$currentRib[2] - 1] = 'L';
                            }
                        } else {
                            if (!isset($outerPlane[(int)$currentRib[1] - 2][(int)$currentRib[2] - 1])) {
                                $outerPlane[(int)$currentRib[1] - 2][(int)$currentRib[2] - 1] = 'R';
                            }
                        }
                    }
                } else {
                    if ($destination == 'right') {
                        if (!isset($innerPlane[(int)$currentRib[1] - 2][(int)$currentRib[2] - 1])) {
                            $innerPlane[(int)$currentRib[1] - 2][(int)$currentRib[2] - 1] = 'L';
                        }
                        if (!isset($outerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1])) {
                            $outerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1] = 'R';
                        }
                    } else {
                        if (!isset($outerPlane[(int)$currentRib[1] - 2][(int)$currentRib[2] - 1])) {
                            $outerPlane[(int)$currentRib[1] - 2][(int)$currentRib[2] - 1] = 'R';
                        }
                        if (!isset($innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1])) {
                            $innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1] = 'L';
                        }
                    }
                }
            } //если оно вертикальное
            else {
                //смотрим, граничное ли оно
                if ($currentRib[2] == $n || $currentRib[2] == 1) {
                    if ($currentRib[2] == 1) {
                        if ($destination == 'up') {
                            if (!isset($outerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1])) {
                                $outerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1] = 'R';
                            }
                        } else {
                            if (!isset($innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1])) {
                                $innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1] = 'L';
                            }
                        }
                    } else {
                        if ($destination == 'up') {
                            if ($currentRib[1] == 1) {
                                if (!isset($innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 2])) {
                                    $innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 2] = 'L';
                                }
                            } else {
                                //echo $currentRib[2]; die('2');
                                if (!isset($innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 2])) {
                                    $innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 2] = 'L';
                                }
                            }
                        } else {
                            if ($currentRib[1] == 1) {
                                if (!isset($outerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 2])) {
                                    $outerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 2] = 'R';
                                }
                            } else {
                                if (!isset($outerPlane[(int)$currentRib[1] - 2][(int)$currentRib[2] - 2])) {
                                    $outerPlane[(int)$currentRib[1] - 2][(int)$currentRib[2] - 2] = 'R';
                                }
                            }
                        }
                    }
                } else {
                    if ($destination == 'up') {
                        if (!isset($innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 2])) {
                            $innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 2] = 'L';
                        }
                        if (!isset($outerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1])) {
                            $outerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1] = 'R';
                        }
                    } else {
                        if (!isset($outerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 2])) {
                            $outerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 2] = 'R';
                        }
                        if (!isset($innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1])) {
                            $innerPlane[(int)$currentRib[1] - 1][(int)$currentRib[2] - 1] = 'L';
                        }
                    }
                }
            }

        }
        $allPlanes = $allPlanes1 = $allPlanes2 = [];
        $innerPlane = $this->fillFields($innerPlane, $outerPlane, $m, $n);
        $outerPlane = $this->fillFields($outerPlane, $innerPlane, $m, $n);
        $allPlanes1 = $this->mergeFields($innerPlane, $outerPlane, $m, $n);
        $allPlanes2 = $this->revertLabyrinth($allPlanes1);
        $allPlanes1 = $this->cutFields($allPlanes1);
        $allPlanes2 = $this->cutFields($allPlanes2);
        $allPlanes = array_merge($allPlanes1, $allPlanes2);

        // проверяем каждую область на условие непротиворечия
        // нужно придумать как каждую особенность хранить отдельно и все вместе
        $final = 0;
        foreach ($allPlanes as $plane) {
            foreach ($types as $type) {
                $final += $this->assert($plane, $specials, $type, $pathString);
            }
        }


        return $final === 0;
    }

    /**
     * @param array $plane
     * @param array $specials
     * @param string $type
     * @param string $pathString
     *
     * @return int
     */
    public function assert(array $plane, array $specials, string $type, string $pathString = ''): int
    {
        /*
         * особенность типа blackwhite - имеется всего 2 точки: чёрная (t2) и белая (t1)
         * нужно провести линию так, чтобы линия отделила все чёрные от всех белых линий
         */
        if ($type == 'blackwhite') {
            $flagTrue = 0;
            foreach ($plane as $k_line => $line) {
                foreach ($line as $k_column => $column) {
                    if (($specials['cellMatrix'][$k_line][$k_column] === LABYRINTH_WHITE_CELL_POINT)
                        || ($specials['cellMatrix'][$k_line][$k_column] === LABYRINTH_BLACK_CELL_POINT)) {
                        if (!isset($already)) {
                            $already = $specials['cellMatrix'][$k_line][$k_column];
                        } else {
                            if ($already != $specials['cellMatrix'][$k_line][$k_column]) {
                                return 1;
                            }
                        }
                    }
                }
            }
        }

        /*
         * особенность типа completepoints:
         * имеется всего множество точек на рёбрах (p),
         * нужно, чтобы линия прошла по всем точкам
         */
        if ($type == 'completepoints') {
            $valH = null;
            $allPath = explode(":", $pathString);
            $needPath = [];
            foreach ($specials['pathMatrix']['h'] as $kh => $h) {
                foreach ($h as $kvalH => $valH) {
                    if ($valH != '0' && $valH != []) {
                        $needPath[] = 'a_' . ($kh + 1) . '_' . ($kvalH + 1);
                    }
                }
            }
            foreach ($specials['pathMatrix']['v'] as $kv => $v) {
                foreach ($v as $kvalV => $valV) {
                    if ($valV != '0' && $valH != []) {
                        $needPath[] = 'b_' . ($kv + 1) . '_' . ($kvalV + 1);
                    }
                }
            }
            $resultPath = [];
            foreach ($allPath as $p) {
                if (in_array($p, $needPath)) {
                    $resultPath[] = $p;
                }
            }

            $flagTrue = count($needPath) === count($resultPath) ? 0 : 1;

            if ($flagTrue === 1) {
                return $flagTrue;
            }
        }

        if ($type == 'countside') {
            $allRibs = explode(":", $pathString);
            $isSidePoint = [
                LABYRINTH_SIDE1_CELL_POINT,
                LABYRINTH_SIDE2_CELL_POINT,
                LABYRINTH_SIDE3_CELL_POINT
            ];
            $countSidePoint = [
                LABYRINTH_SIDE1_CELL_POINT => 1,
                LABYRINTH_SIDE2_CELL_POINT => 2,
                LABYRINTH_SIDE3_CELL_POINT => 3
            ];

            foreach ($specials['cellMatrix'] as $kLine => $line) {
                foreach ($line as $kCell => $cell) {
                    if (in_array($cell, $isSidePoint, true)) {
                        if ($this->getCountRibsOfCellByPath($kLine, $kCell, $allRibs) != $countSidePoint[$cell]) {
                            return 1;
                        }
                    }
                }
            }
        }

        return 0;
    }

    /**
     * @brief Определить, по данным координатам точки и пути, сколько раз путь касается клетки
     *
     * @param int $line
     * @param int $cell
     * @param array $path
     *
     * @return int
     */
    private function getCountRibsOfCellByPath(int $line, int $cell, array $path): int
    {
        $allExistsRibs = [
            'a_' . ($line + 1) . '_' . ($cell + 1),
            'a_' . ($line + 2) . '_' . ($cell + 1),
            'b_' . ($line + 1) . '_' . ($cell + 1),
            'b_' . ($line + 1) . '_' . ($cell + 2)
        ];

        $count = 0;
        foreach ($allExistsRibs as $r) {
            if (in_array($r, $path)) {
                $count++;
            }
        }

        return $count;
    }
}
