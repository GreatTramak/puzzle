<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Modules\GeneratorModule;

/**
 * Class CronController
 *
 * @package App\Http\Controllers
 */
class CronController extends Controller
{
    use \App\Http\Traits\Helper;

    /**
     * @var int $maxM
     */
    private $maxM = 10;

    /**
     * @var int $maxN
     */
    private $maxN = 10;

    /**
     * @brief генерация множества лабиринтов для определённой группы
     *
     * @return void
     */
    public function generateAction(): void
    {
        // этот блок генерирует все разновидности размерностей
        // и комбинаций типов, чтобы потом передавать конкретную
        // запись для генерации

        /*
        //die('not working yet');

        die('3');

        $query = 'SELECT slug FROM labyrinth_types WHERE status = 1';
        $types = DB::select($query);
        $types = $this->object_to_array($types);
        $typesArray = [];
        foreach ($types as $t) {
            $typesArray[] = $t['slug'];
        }
        */
        //$query = 'SELECT * FROM generator_labyrinth WHERE generated IS FALSE LIMIT 1';
        $query = 'SELECT * FROM generator_labyrinth WHERE id = 298 LIMIT 1';
        $generate = DB::select($query);

        if (!empty($generate)) {
            $generate = $this->objectToArray($generate);
            $t = unserialize($generate[0]['types']);
            $generatorModule = new GeneratorModule();
            $all = $generatorModule->generateByParams($generate[0]['m'],
                $generate[0]['n'], $t, $generate[0]['region']);

            DB::table('generator_labyrinth')
                ->where("id", "=", $generate[0]['id'])
                ->update(['generated' => true]);
            print($all);
        }
    }

    /**
     * @brief Генерация типов либиринтов и всех их всевозможных комбинаций
     * COMPLETED
     *
     * @return void
     */
    public function generateGroupAction(): void
    {
        $types = $this->objectToArray(DB::table('labyrinth_types')->where('status', 1)->get()->all());
        $generatorModule = new GeneratorModule();

        $allTypes = $generatorModule->generateSpesialsArray($types);

        foreach ($allTypes as $type) {
            // формируем массив типов лабиринтов
            $currentTypes = [];
            foreach ($type as $t) {
                $currentTypes[] = $t['slug'];
            }
            // находим номер проставляемого региона
            $isExistRegion = DB::table('generator_labyrinth')
                ->where('types', serialize($currentTypes))->limit(1)->get()->all();

            if (count($isExistRegion) > 0) {
                $region = $isExistRegion[0]->region;
            } else {
                $region = null;
            }

            for ($i = 1; $i <= $this->maxM; $i++) {
                for ($j = 1; $j <= $this->maxN; $j++) {
                    if ($i === 1 && $j == 1) {
                        continue;
                    }

                    $isExist = DB::table('generator_labyrinth')
                        ->where('m', $i)
                        ->where('n', $j)
                        ->where('types', serialize($currentTypes))
                        ->get()
                        ->all();

                    if (count($isExist) > 0) {
                        continue;
                    } else {
                        if (!$region) {
                            $region = DB::table('generator_labyrinth')->where('status', 1)->orderBy('region')
                                ->limit(1)->get()->all()[0]->region;
                        }
                        $sqlParams = [
                            'm' => $i,
                            'n' => $j,
                            'types' => serialize($currentTypes),
                            'generated' => false,
                            'region' => $region
                        ];
                        DB::table('generator_labyrinth')->insert($sqlParams);

                        return;
                    }
                }
            }
        }
    }

    /**
     * @brief Тестирование работоспособности крона
     * COMPLETED
     *
     * @return string
     */
    public function testAction()
    {
        return 'pong';
    }

    /**
     * @brief Очистка таблиц головоломок (ДЛЯ ДЕБАГА - НА ПРОДЕ ЗАПРЕТИТЬ ИЛИ УДАЛИТЬ)
     *
     * @return void
     */
    public function clearDatabaseAction(): void
    {
        // Убираем связи лабиринтов и их типов
        $query = 'DELETE FROM labyrinth_types_relations WHERE TRUE';
        DB::select($query);

        // Убираем все лабиринты
        $query = 'DELETE FROM labyrinths WHERE TRUE';
        DB::select($query);

        // Убираем головоломки, которые прошёл пользователь
        $query = 'DELETE FROM users_puzzles WHERE TRUE';
        DB::select($query);

        // Убираем головоломки
        $query = 'DELETE FROM puzzles WHERE TRUE';
        DB::select($query);

        die('clean!');
    }
}
