<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Modules\ImageModule;

/**
 * Class ImageController
 *
 * @package App\Http\Controllers
 */
class ImageController extends Controller
{
    use \App\Http\Traits\Helper;

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function backgroundImage(Request $request)
    {
        //TODO Вынести и сделать красиво!!!
        $maps = $this->objectToArray(DB::table('maps')->where('status', 1)->get()->all());
        $regions = $this->objectToArray(DB::table('regions')->where('status', 1)->get()->all());
        $colors = $this->objectToArray(DB::table('region_colors')->where('status', 1)->get()->all());

        foreach ($regions as $kr => $r) {
            $regions[$kr]['colors'] = [];
            foreach ($colors as $c) {
                if ($c['region_id'] == $r['id']) {
                    $regions[$kr]['colors'][] = $c;
                }
            }
        }

        foreach ($maps as $km => $m) {
            $maps[$km]['regions'] = [];
            foreach ($regions as $r) {
                if ($r['map_id'] == $m['id']) {
                    $maps[$km]['regions'][] = $r;
                }
            }
        }

        $region = (int)$request->input('region');
        $imageModule = new ImageModule();
        $color = $imageModule->getRegionFromDataById($region, $maps);

        if (!$color || !is_array($color)) {
            $color = ['r' => 'ff', 'g' => 'ff', 'b' => 'ff'];
        }

        $url = './screensavers/' . $region . '/';
        $files = [];
        $dir = (array)scandir($url);
        foreach ($dir as $filename) {
            if (in_array($filename, ['.', '..'])) {
                continue;
            }
            $files[] = $filename;
        }
        $file = $files[rand(0, count($files) - 1)];

        $img = imagecreatefromjpeg($url . $file);

        if (false !== $img) {
            imagefilter($img, IMG_FILTER_COLORIZE, $color['r'], $color['g'], $color['b'], 50);
            $img = $imageModule->addEffectByRegionCategory($img, 1);

            header('Content-Type: image/jpeg');
            imagejpeg($img);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        //
    }
}
