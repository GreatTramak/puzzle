<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use \Illuminate\Support\Facades\{
    Auth, DB
};
use App\Models\{
    Labyrinth, Puzzle
};
use App\Http\Modules\{
    LabyrinthModule, GeneratorModule, UserModule
};

/**
 * Class LabyrinthController
 *
 * @package App\Http\Controllers
 */
class LabyrinthController extends Controller
{
    use \App\Http\Traits\Helper;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @brief дефолтный экшен
     *
     * @param array $params
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index($params)
    {
        $user = Auth::user()->getAttributes();

        $query = 'SELECT * '
            . 'FROM puzzles as p '
            . 'WHERE '
            . 'p.id NOT IN ('
            . 'SELECT puzzle_id '
            . 'FROM users_puzzles '
            . 'WHERE user_id = ' . $user['id'] . ') '
            . ' AND p.status = 1 '
            . ' AND p.category_puzzle_id = 1 '
            . ' AND p.difficult <= ' . $user['level']
            . ' AND p.region_id = ' . ((int)$params['region_id'])
            . ' ORDER BY p.id DESC LIMIT 10';

        $puzzles = DB::select($query);
        $puzzles = $this->objectToArray($puzzles);
        if (count($puzzles) > 0) {
            shuffle($puzzles);
            $current = $puzzles[0];
            return redirect($_SERVER['REQUEST_URI'] . "/" . $current['hash']);
        } else {
            die('This will be page about new puzzle will coming soon');
        }
    }

    /**
     * @brief Отображение списка решённых лабиринтов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexCompleted()
    {
        $query = 'SELECT * '
            . 'FROM `puzzles` as `p` '
            . 'WHERE '
            . '`p`.`id` IN ('
            . 'SELECT `puzzle_id` '
            . 'FROM `users_puzzles` '
            . 'WHERE `user_id` = ' . Auth::user()['attributes']['id'] . ') '
            . ' AND `p`.`status` = 1 '
            . ' ORDER BY `p`.`id` DESC';

        $puzzles = DB::select($query);
        $puzzles = $this->objectToArray($puzzles);
        $params = [
            'labyrinths' => $puzzles
        ];

        return view('puzzle.labyrinth.completed', ['params' => $params]);
    }

    /**
     * @brief Контроллер отображения
     *
     * @param array $p
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function view($p)
    {
        $puzzle = Puzzle::where('hash', '=', $p['hash'])->get()->toArray()[0];
        $labyrinth = Labyrinth::where('id', '=', $puzzle['item_id'])->get()->toArray()[0];
        $lab = new LabyrinthModule($labyrinth);

        $pathString = '';

        $cellLength = $lab->calculateCellLength();
        $offsetX = $lab->calculateLabyrinthOffsetHorizont();
        $offsetY = $lab->calculateLabyrinthOffsetVertical();

        $params = [
            'm' => $lab->m,
            'n' => $lab->n,
            'types' => $p['types'],
            'specials' => json_decode($lab->specials),
            'pathString' => $pathString,
            'accessible_way' => json_decode($lab->accessible_way),
            'cellLength' => $cellLength,
            'offsetX' => $offsetX,
            'offsetY' => $offsetY,
            'shatter' => 30,
            'id' => $puzzle['id'],
            'region_id' => (int)$p['region_id']
        ];

        $params = $this->objectToArray($params);

        if (isset($_GET['debug']) && (int)$_GET['debug'] === 1) {
            print("<pre>");
            print_r($params);
            print("</pre>");
        }

        return view('puzzle.labyrinth.view', ['params' => $params]);
    }

    /**
     * @param Request $request
     *
     * @return string|false
     */
    public function resolve(Request $request)
    {
        $labyrinthModule = new LabyrinthModule();

        $types = explode(',', $request->input('types'));
        $specials = json_decode($request->input('specials'));
        $specials = $this->objectToArray($specials);
        $path = $request->input('path');
        $result = $labyrinthModule->isSolved(
            $request->input('m'),
            $request->input('n'),
            $path,
            $types,
            $specials
        );
        if ($result) {
            // сохранение связи лабиринта, решения и пользователя
            $sqlParams = [
                'puzzle_id' => (int)$request->input('id'),
                'solution' => $path,
                'status' => 1
            ];
            $userModule = new UserModule();
            $userModule->completePuzzle(Auth::user(), $sqlParams);
        }

        $response = [
            'result' => $result
        ];

        return json_encode($response);
    }

    /**
     * @param array $params
     *
     * @return void
     */
    public function save(array $params)
    {
        $m = $params['m'];
        $n = $params['n'];

        $labyrinthModule = new LabyrinthModule();

        $difficult = floor(($m * $n) / 10) + 1;

        $specials = $params['specials'];

        foreach ($specials['end'] as $keyEnd => $endUnit) {
            $delta = $labyrinthModule->calculateEndLine($endUnit, ['m' => ($m + 1), 'n' => ($n + 1)]);
            $specials['end'][$keyEnd] = array_merge($specials['end'][$keyEnd], $delta);
        }

        $accessible_way = [];
        $accessible_way['horizont'] = $params['accessible_way']['horizont'];
        $accessible_way['vertical'] = $params['accessible_way']['vertical'];
        $solution = $params['solution'];

        $regionId = (int)$params['region'];

        $categoryPuzzleId = 1;

        $labyrinth = Labyrinth::create([
            'm' => $m,
            'n' => $n,
            'specials' => (string)json_encode($specials),
            'solution' => $solution,
            'accessible_way' => (string)json_encode($accessible_way)
        ]);
        $lab = $labyrinth->id;

        // сохранение связи лабиринта и его типа
        $sqlParams = ['labyrinth_id' => $lab, 'type_id' => 1, 'status' => 1];
        DB::table('labyrinth_types_relations')->insert($sqlParams);

        // сохранение связи лабиринта и головоломки
        Puzzle::create([
            'item_id' => $lab,
            'category_puzzle_id' => 1,
            'difficult' => (int)$difficult,
            'region_id' => $regionId,
            'status' => 1,
            'hash' => md5($categoryPuzzleId . "|" . $lab)
        ]);
    }

    /**
     * @brief Решатель головоломок
     *
     * @return void
     */
    public function solutor(): void
    {
        $g = new GeneratorModule();
        $labyrinthModule = new LabyrinthModule();

        $query = 'SELECT `p`.`hash`, `l`.* '
            . 'FROM `labyrinths` as `l` '
            . 'LEFT JOIN `puzzles` AS `p` ON `l`.`id` = `p`.`item_id`'
            . 'WHERE `l`.`solution` = ""'
            . ' ORDER BY `l`.`id` DESC LIMIT 500000';

        $puzzles = DB::select($query);
        $puzzles = $this->objectToArray($puzzles);

        $solve = '';
        foreach ($puzzles as $p) {
            $delete = false;
            $ways = json_decode($p['accessible_way']);
            $spec = json_decode($p['specials']);
            $specials = [
                'dim' => [
                    'H' => $ways->horizont,
                    'V' => $ways->vertical
                ],
                'starts' => $spec->start,
                'ends' => $spec->end
            ];
            $sp = [
                'cellMatrix' => $spec->cellMatrix,
                'pathMatrix' => $spec->pathMatrix
            ];

            $allSolves = $g->generateAllWays($specials);
            if ($allSolves) {
                foreach ($allSolves as $solve) {
                    if ($labyrinthModule->isSolved($p['m'], $p['n'], $solve, ['blackwhite'], $sp)) {
                        print_r("yes:" . $p['hash'] . "-" . $solve . "<br/>");
                    } else {
                        print_r("no:" . $p['id'] . "<br/>");
                        $delete = true;
                    }
                }
            } else {
                print_r("no:" . $p['id'] . "<br/>");
                $delete = true;
            }
            if ($delete === true) {
                DB::table('labyrinth_types_relations')
                    ->where('labyrinth_id', "=", $p['id'])
                    ->delete();
                DB::table("puzzles")
                    ->where('item_id', "=", $p['id'])
                    ->where("category_puzzle_id", "=", 1)
                    ->delete();
                DB::table('labyrinths')
                    ->where('id', '=', $p['id'])
                    ->delete();
            } else {
                DB::table('labyrinths')
                    ->where("id", "=", $p['id'])
                    ->update(['solution' => $solve]);
            }
        }
    }
}
