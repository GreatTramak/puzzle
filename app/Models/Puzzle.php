<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Puzzle
 *
 * @package App\Models
 */
class Puzzle extends Model
{
    /**
     * @var $timestamps
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array $fillable
     */
    protected $fillable = [
        'item_id', 'category_puzzle_id', 'difficult', 'region_id', 'status', 'hash'
    ];
}
