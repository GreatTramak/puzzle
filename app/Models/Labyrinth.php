<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Labyrinth
 *
 * @package App\Models
 */
class Labyrinth extends Model
{
    /**
     * @var $timestamps
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array $fillable
     */
    protected $fillable = [
        'm', 'n', 'specials', 'solution', 'accessible_way'
    ];
}
