<?php
    // путь до всех контроллеров в приложении
    define('CONTROLLERS_PATH', 'App\Http\Controllers\\');
    
    // константы особенностей лабиринтов
    define('LABYRINTH_WHITE_CELL_POINT', 't1');
    define('LABYRINTH_BLACK_CELL_POINT', 't2');
    
    define('LABYRINTH_RED_RIB_POINT', 'p');
    
    define('LABYRINTH_SIDE1_CELL_POINT', 's1');
    define('LABYRINTH_SIDE2_CELL_POINT', 's2');
    define('LABYRINTH_SIDE3_CELL_POINT', 's3');
?>