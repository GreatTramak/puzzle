<?php
// Набор всяких вспомогательных функций

/**
 * @brief Вывод данных в читабельном формате
 *
 * @param mixed $data
 *
 * @return void
 */
function pr($data): void
{
    print("<pre>");
    print_r($data);
    print("</pre>");
}

?>
