<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Http\Modules\LabyrinthModule;
use Tests\TestCase;

/**
 * Class LabyrinthModuleTest
 */
class LabyrinthModuleTest extends TestCase
{
    /**
     * @brief Проверка метода заполнения лабиринтов на левые и правые части
     *
     * @return void
     */
    public function testFillFields()
    {
        $LM = new LabyrinthModule();

        $labyrinth = [[1 => 'L']];
        $secondLabyrinth = [[0 => 'R']];
        $m = 1;
        $n = 3;
        $expectedResult = [[1 => 'L', 2 => 'L']];
        $result = $LM->fillFields($labyrinth, $secondLabyrinth, $m + 1, $n + 1);

        $this->assertEquals($result, $expectedResult, 'fill m=1 n=3');
    }

    /**
     * @brief Проверка метода решателя лабиринтов.
     * Тип:
     *      1) чёрные и белые точки на клетках
     *      2) прохожждение лабиринта по точкам
     *
     * @return void
     */
    public function testIsSolvedCompletePointsBlackAndWhiteType()
    {
        $LC = new LabyrinthModule();

        $types = ['blackwhite', 'completepoints'];

        // 1
        $m = 4;
        $n = 4;

        $specials['cellMatrix'] = [
            [0, 0, LABYRINTH_WHITE_CELL_POINT, 0],
            [0, LABYRINTH_BLACK_CELL_POINT, 0, 0],
            [0, 0, LABYRINTH_BLACK_CELL_POINT, 0],
            [LABYRINTH_WHITE_CELL_POINT, 0, 0, 0]
        ];
        $specials['pathMatrix'] = [
            'h' => [
                [0, 0, 0, 'p'],
                [0, 'p', 0, 0],
                [0, 'p', 0, 0],
                [0, 0, 0, 0],
                [0, 0, 'p', 0]
            ],
            'v' => [
                [0, 0, 'p', 0, 0],
                [0, 'p', 0, 0, 0],
                [0, 0, 0, 'p', 0],
                [0, 'p', 'p', 0, 0]
            ]
        ];

        $pathString = 'a_5_1:b_4_2:a_4_2:b_4_3:a_5_3:b_4_4:b_3_4:a_3_3:a_3_2:b_2_2:a_2_2:b_1_3:a_1_3:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '1th solve');

        // 2
        $pathString = 'b_4_1:a_4_1:a_4_2:b_4_3:a_5_3:b_4_4:b_3_4:a_3_3:a_3_2:b_2_2:a_2_2:b_1_3:a_1_3:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '2th solve');

        // 3
        $specials['cellMatrix'] = [
            [0, 0, LABYRINTH_BLACK_CELL_POINT, 0],
            [0, LABYRINTH_BLACK_CELL_POINT, 0, 0],
            [0, 0, LABYRINTH_WHITE_CELL_POINT, 0],
            [LABYRINTH_WHITE_CELL_POINT, 0, 0, 0]
        ];
        $specials['pathMatrix'] = [
            'h' => [
                [0, 0, 0, 'p'],
                [0, 'p', 0, 0],
                [0, 'p', 0, 0],
                [0, 0, 0, 0],
                [0, 0, 'p', 0]
            ],
            'v' => [
                [0, 0, 'p', 0, 0],
                [0, 'p', 0, 0, 0],
                [0, 0, 0, 'p', 0],
                [0, 'p', 'p', 0, 0]
            ]
        ];

        $pathString = 'a_5_1:b_4_2:a_4_2:b_4_3:a_5_3:b_4_4:b_3_4:a_3_3:a_3_2:b_2_2:a_2_2:b_1_3:a_1_3:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '3rd solve');

        // 4
        $m = 3;
        $n = 1;
        $specials['cellMatrix'] = [
            [LABYRINTH_WHITE_CELL_POINT],
            [LABYRINTH_WHITE_CELL_POINT],
            [LABYRINTH_BLACK_CELL_POINT]
        ];
        $specials['pathMatrix'] = [
            'h' => [
                [0],
                [0],
                ['p'],
                [0]
            ],
            'v' => [
                [0, 'p'],
                [0, 0],
                ['p', 0]
            ]
        ];

        $pathString = 'b_3_1:a_3_1:b_2_2:b_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, 'fourth solve');

        // 5
        $specials['cellMatrix'] = [
            [LABYRINTH_WHITE_CELL_POINT],
            [LABYRINTH_WHITE_CELL_POINT],
            [LABYRINTH_BLACK_CELL_POINT]
        ];
        $specials['pathMatrix'] = [
            'h' => [
                [0],
                ['p'],
                [0],
                [0]
            ],
            'v' => [
                [0, 'p'],
                [0, 0],
                ['p', 0]
            ]
        ];

        $pathString = 'b_3_1:a_3_1:b_2_2:b_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, 'fifth solve');
    }

    /**
     * @brief Проверка метода решателя лабиринтов.
     * Тип:
     *      1) чёрные и белые точки на клетках
     *
     * @return void
     */
    public function testIsSolvedBlackWhiteType()
    {
        $LC = new LabyrinthModule();

        $types = ['blackwhite'];

        $m = 4;
        $n = 4;
        $specials = [];
        $specials['pathMatrix'] = [];
        $specials['cellMatrix'] = [
            [0, 0, LABYRINTH_WHITE_CELL_POINT, 0],
            [0, LABYRINTH_BLACK_CELL_POINT, 0, 0],
            [0, 0, LABYRINTH_BLACK_CELL_POINT, 0],
            [LABYRINTH_WHITE_CELL_POINT, 0, 0, 0]
        ];

        // 1
        $pathString = 'a_5_1:b_4_2:b_3_2:b_2_2:a_2_2:a_2_3:b_1_4:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, 'first solve');

        // 2
        $pathString = 'b_4_1:a_4_1:a_4_2:a_4_3:a_4_4:b_3_5:b_2_5:b_1_5';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue(!$solved, 'second solve');

        // 3
        $pathString = 'b_4_1:a_4_1:a_4_2:a_4_3:b_3_4:b_2_4:b_1_4:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, 'third solve');

        // 4
        $pathString = 'a_5_1:b_4_2:a_4_1:b_3_1:b_2_1:b_1_1:a_1_1:a_1_2:b_1_3:a_2_3:b_1_4:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, 'fourth solve');

        // 5
        $pathString = 'b_4_1:b_3_1:b_2_1:b_1_1:a_1_1:a_1_2:a_1_3:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, 'fifth solve');

        //////////////////////////////////////////////////////////////////////////////////////////

        $specials['cellMatrix'] = [
            [LABYRINTH_WHITE_CELL_POINT], [LABYRINTH_BLACK_CELL_POINT]
        ];
        // 6
        $m = 2;
        $n = 1;
        $pathString = 'a_2_1';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, 'six solve');

        // 7
        $pathString = 'b_1_1:a_1_1:b_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, 'seven solve');

        // 8
        $pathString = 'b_2_1:a_3_1:b_2_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, 'eight solve');

        // 9
        $pathString = 'b_2_1:a_2_1:b_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, 'nine solve');

        // 10
        $pathString = 'a_3_1:b_2_2:b_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, 'ten solve');

        // 11
        $pathString = 'b_2_1:b_1_1:a_1_1';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, 'eleven solve');

        ///////////////////////////////////////////////////////////////////////////////////////////
        $specials['cellMatrix'] = [
            [LABYRINTH_WHITE_CELL_POINT],
            [LABYRINTH_WHITE_CELL_POINT],
            [LABYRINTH_BLACK_CELL_POINT]
        ];

        // 12
        $m = 3;
        $n = 1;
        $pathString = 'b_3_1:a_3_1:b_2_2:b_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, 'twelve solve');

        // 13
        $pathString = 'a_4_1:b_3_2:b_2_2:b_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '13th solve');

        // 14
        $pathString = 'b_3_1:a_3_1:b_2_2:a_2_1:b_1_1:a_1_1';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '14th solve');

        ///////////////////////////////////////////////////////////////////////////////////////////
        $specials['cellMatrix'] = [
            [LABYRINTH_WHITE_CELL_POINT, LABYRINTH_WHITE_CELL_POINT],
            [LABYRINTH_WHITE_CELL_POINT, LABYRINTH_BLACK_CELL_POINT]
        ];

        // 15
        $m = 2;
        $n = 2;
        $pathString = 'a_3_1:b_2_2:a_2_2:b_1_3';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '15th solve');

        // 16
        $pathString = 'a_3_1:b_2_2:b_1_2:a_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '16th solve');

        // 17
        $pathString = 'b_2_1:b_1_1:a_1_1:a_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '17th solve');

        // 18
        $pathString = 'b_2_1:a_2_1:a_2_2:b_1_3';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue(!$solved, '18th solve');

        // 19
        $pathString = 'a_3_1:a_3_2:b_2_3:b_1_3';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '19th solve');

        // 20
        $pathString = 'b_2_1:b_1_1:a_1_1:b_1_2:b_2_2:a_3_2:b_2_3:b_1_3';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '20th solve');

        ///////////////////////////////////////////////////////////////////////////////////////////
        $specials['cellMatrix'] = [[LABYRINTH_WHITE_CELL_POINT, LABYRINTH_BLACK_CELL_POINT]];

        // 21
        $m = 1;
        $n = 2;
        $pathString = 'a_2_1:b_1_2:a_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '21th solve');

        // 22
        $pathString = 'a_2_1:a_2_2:b_1_3';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '22th solve');

        // 23
        $pathString = 'b_1_1:a_1_1:a_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '23th solve');

        // 24
        $pathString = 'b_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '24th solve');

        // 25
        $pathString = 'a_2_1:b_1_1:a_1_1';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '25th solve');

        // 26
        $pathString = 'a_2_2:b_1_3:a_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '26th solve');

        ///////////////////////////////////////////////////////////////////////////////////////////
        $specials['cellMatrix'] = [
            [LABYRINTH_BLACK_CELL_POINT, LABYRINTH_BLACK_CELL_POINT, LABYRINTH_BLACK_CELL_POINT],
            [LABYRINTH_BLACK_CELL_POINT, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_BLACK_CELL_POINT],
            [LABYRINTH_WHITE_CELL_POINT, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_WHITE_CELL_POINT]
        ];

        // 27
        $m = 3;
        $n = 3;
        $pathString = 'b_3_1:a_3_1:b_2_2:a_2_2:b_2_3:a_3_3:b_2_4:b_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '27th solve');

        // 28
        $pathString = 'b_3_1:a_3_1:b_3_2:a_4_2:a_4_3:b_3_4:a_3_3:b_2_3:a_2_2:a_2_1:b_1_1:a_1_1:a_1_2:a_1_3';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '28th solve');

        // 29
        $pathString = 'a_4_1:a_4_2:a_4_3:b_3_4:a_3_3:b_2_3:a_2_2:b_2_2:a_3_1';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '29th solve');

        ///////////////////////////////////////////////////////////////////////////////////////////
        $specials['cellMatrix'] = [
            [LABYRINTH_BLACK_CELL_POINT, 0, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_BLACK_CELL_POINT],
            [LABYRINTH_BLACK_CELL_POINT, LABYRINTH_BLACK_CELL_POINT, LABYRINTH_BLACK_CELL_POINT, LABYRINTH_BLACK_CELL_POINT],
            [LABYRINTH_BLACK_CELL_POINT, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_BLACK_CELL_POINT, LABYRINTH_BLACK_CELL_POINT],
            [LABYRINTH_WHITE_CELL_POINT, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_BLACK_CELL_POINT]
        ];

        // 30
        $m = 4;
        $n = 4;
        $pathString = 'b_4_1:a_4_1:b_3_2:a_3_2:b_3_3:a_4_3:b_4_4:a_5_4:b_4_5:b_3_5:b_2_5:b_1_5:a_1_4:b_1_4:a_2_3:a_2_2:b_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '30th solve');

        // 31
        $pathString = 'b_4_1:a_4_1:b_3_2:a_3_2:b_2_3:a_2_2:b_1_2';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '31th solve');

        ///////////////////////////////////////////////////////////////////////////////////////////
        $specials['cellMatrix'] = [
            [LABYRINTH_BLACK_CELL_POINT, LABYRINTH_BLACK_CELL_POINT, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_BLACK_CELL_POINT],
            [0, LABYRINTH_BLACK_CELL_POINT, LABYRINTH_BLACK_CELL_POINT, LABYRINTH_BLACK_CELL_POINT],
            [LABYRINTH_BLACK_CELL_POINT, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_BLACK_CELL_POINT, LABYRINTH_BLACK_CELL_POINT],
            [LABYRINTH_WHITE_CELL_POINT, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_BLACK_CELL_POINT]
        ];

        // 32
        $pathString = 'b_4_1:a_4_1:b_3_2:a_3_2:b_3_3:a_4_3:b_4_4:a_5_4:b_4_5:b_3_5';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '32th solve');

        // 33
        $pathString = 'a_5_1:a_5_2:a_5_3:b_4_4:a_4_3:b_3_3:a_3_2:b_3_2:a_4_1:b_3_1:b_2_1:b_1_1:a_1_1:a_1_2:b_1_3:a_2_3:b_1_4:a_1_4:b_1_5:b_2_5';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '33th solve');

        ///////////////////////////////////////////////////////////////////////////////////////////
        $specials['cellMatrix'] = [
            [0, LABYRINTH_BLACK_CELL_POINT, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_BLACK_CELL_POINT],
            [LABYRINTH_BLACK_CELL_POINT, 0, 0, LABYRINTH_BLACK_CELL_POINT],
            [LABYRINTH_BLACK_CELL_POINT, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_BLACK_CELL_POINT, 0],
            [LABYRINTH_WHITE_CELL_POINT, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_WHITE_CELL_POINT, LABYRINTH_BLACK_CELL_POINT]
        ];

        // 34
        $pathString = 'b_4_1:a_4_1:b_3_2:b_2_2:a_2_2:b_1_3:a_1_3:b_1_4:b_2_4:a_3_3:b_3_3:a_4_3:b_4_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '34th solve');

        // 35
        $m = 1;
        $n = 3;
        $specials['cellMatrix'] = [[LABYRINTH_WHITE_CELL_POINT, LABYRINTH_BLACK_CELL_POINT, LABYRINTH_WHITE_CELL_POINT]];
        $pathString = 'a_1_2:b_1_2:a_2_1:b_1_1';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '35th solve');
    }

    /**
     * Проверка метода решателя лабиринтов.
     * Тип:
     *      1) прохожждение лабиринта по точкам
     *
     * @return void
     */
    public function testIsSolvedCompletePointsType(): void
    {
        $LC = new LabyrinthModule();

        $types = ['completepoints'];

        $specials['cellMatrix'] = [];
        $specials['pathMatrix'] = [
            'h' => [
                [0, 0, 0, 'p'],
                [0, 'p', 0, 0],
                [0, 'p', 0, 0],
                [0, 0, 0, 0],
                [0, 0, 'p', 0]
            ],
            'v' => [
                [0, 0, 'p', 0, 0],
                [0, 'p', 0, 0, 0],
                [0, 0, 0, 'p', 0],
                [0, 'p', 'p', 0, 0]
            ]
        ];
        // 1
        $m = 4;
        $n = 4;
        $pathString = 'a_5_1:b_4_2:a_4_2:b_4_3:a_5_3:b_4_4:b_3_4:a_3_3:a_3_2:b_2_2:a_2_2:b_1_3:a_1_3:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '1th solve');

        // 2
        $pathString = 'b_4_1:a_4_1:a_4_2:b_4_3:a_5_3:b_4_4:b_3_4:a_3_3:a_3_2:b_2_2:a_2_2:b_1_3:a_1_3:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '2th solve');

        // 3
        $specials['pathMatrix'] = [
            'h' => [
                [[], 'p', []],
                ['p', [], []],
            ],
            'v' => [
                [[], [], [], []],
            ]
        ];
        $m = 1;
        $n = 3;
        $pathString = 'a_2_1:b_1_2:a_1_2:a_1_3';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '3th solve');
    }

    /**
     * Проверка метода решателя лабиринтов.
     * Тип:
     *      1) точки по количеству окружающих сторон на клетках
     *
     * @return void
     */
    public function testIsSolvedCountSideType(): void
    {
        $LC = new LabyrinthModule();

        $types = ['countside'];

        $m = 4;
        $n = 4;
        $specials = [];
        $specials['pathMatrix'] = [];
        $specials['cellMatrix'] = [
            [0, LABYRINTH_SIDE1_CELL_POINT, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ];

        // 1
        $pathString = 'a_5_1:b_4_2:b_3_2:b_2_2:a_2_2:a_2_3:b_1_4:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, 'first solve');

        // 2
        $specials['cellMatrix'] = [
            [0, LABYRINTH_SIDE2_CELL_POINT, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ];

        $pathString = 'a_5_1:b_4_2:b_3_2:b_2_2:a_2_2:a_2_3:b_1_4:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, 'second solve');

        // 3
        $specials['cellMatrix'] = [
            [0, LABYRINTH_SIDE2_CELL_POINT, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ];

        $pathString = 'a_5_1:b_4_2:b_3_2:b_2_2:a_2_2:b_1_3:a_1_3:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, 'third solve');

        // 4
        $specials['cellMatrix'] = [
            [0, LABYRINTH_SIDE3_CELL_POINT, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ];

        $pathString = 'a_5_1:b_4_2:b_3_2:a_3_1:b_2_1:b_1_1:a_1_1:b_1_2:a_2_2:b_1_3:a_1_3:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '4th solve');

        // 5
        $specials['cellMatrix'] = [
            [0, LABYRINTH_SIDE3_CELL_POINT, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ];

        $pathString = 'a_5_1:b_4_2:b_3_2:a_3_1:b_2_1:b_1_1:a_1_1:b_1_2:a_2_2:a_2_3:b_1_4:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '5th solve');

        // 6
        $specials['cellMatrix'] = [
            [0, LABYRINTH_SIDE3_CELL_POINT, LABYRINTH_SIDE2_CELL_POINT, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ];

        $pathString = 'a_5_1:b_4_2:b_3_2:a_3_1:b_2_1:b_1_1:a_1_1:b_1_2:a_2_2:b_1_3:a_1_3:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '6th solve');

        // 7
        $specials['cellMatrix'] = [
            [0, LABYRINTH_SIDE3_CELL_POINT, LABYRINTH_SIDE3_CELL_POINT, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ];

        $pathString = 'a_5_1:b_4_2:b_3_2:a_3_1:b_2_1:b_1_1:a_1_1:b_1_2:a_2_2:b_1_3:a_1_3:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '7th solve');

        // 8
        $specials['cellMatrix'] = [
            [0, LABYRINTH_SIDE3_CELL_POINT, LABYRINTH_SIDE1_CELL_POINT, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ];

        $pathString = 'a_5_1:b_4_2:b_3_2:a_3_1:b_2_1:b_1_1:a_1_1:b_1_2:a_2_2:b_1_3:a_1_3:a_1_4';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '8th solve');

        // 9
        $m = 3;
        $n = 3;
        $specials['cellMatrix'] = [
            [0, 0, 0],
            [0, LABYRINTH_SIDE1_CELL_POINT, 0],
            [0, 0, 0]
        ];

        $pathString = 'a_4_1:b_3_2:b_2_2:b_1_2:a_1_2:a_1_3';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '9th solve');

        // 10
        $specials['cellMatrix'] = [
            [0, 0, 0],
            [0, LABYRINTH_SIDE2_CELL_POINT, 0],
            [0, 0, 0]
        ];

        $pathString = 'a_4_1:b_3_2:b_2_2:b_1_2:a_1_2:a_1_3';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertFalse($solved, '10th solve');

        // 11
        $specials['cellMatrix'] = [
            [0, 0, 0],
            [0, LABYRINTH_SIDE2_CELL_POINT, 0],
            [0, 0, 0]
        ];

        $pathString = 'a_4_1:b_3_2:b_2_2:a_2_2:b_1_3:a_1_3';
        $solved = $LC->isSolved($m, $n, $pathString, $types, $specials);
        $this->assertTrue($solved, '10th solve');
    }

    /**
     * Промежуточный тест для дебага
     *
     * @return void
     */
    public function testIsSolvedTemp(): void
    {
        $this->assertTrue(true, 'complete!');
    }
}
