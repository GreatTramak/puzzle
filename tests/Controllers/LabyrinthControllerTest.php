<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Http\Controllers\LabyrinthController;
use Tests\TestCase;

/**
 * Class LabyrinthControllerTest
 */
class LabyrinthControllerTest extends TestCase
{
    //vendor\bin\phpunit tests\LabyrinthControllerTest.php

    /**
     * LabyrinthController->indexCompleted
     * Test Case: call as guest
     *
     * @return void
     */
    public function testIndexCompletedActionNonAuthorizedUser()
    {
        $response = $this->get('/labyrinth/completed/');
        $response->assertRedirect('login');
    }
}
